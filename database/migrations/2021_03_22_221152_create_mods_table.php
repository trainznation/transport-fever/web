<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mods', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('mod_id');
            $table->string('mod_version');
            $table->integer('state')->default(0)->comment("0: Alpha |1: Beta |2: Final");
            $table->integer('mod_size')->nullable();
            $table->text('description');
            $table->integer('published')->default(0)->comment("0: Not Publish |1: Publish Later |2: Published");
            $table->timestamp('published_at')->nullable();
            $table->timestamps();

            $table->foreignId('mod_category_id')->constrained()->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mods');
    }
}
