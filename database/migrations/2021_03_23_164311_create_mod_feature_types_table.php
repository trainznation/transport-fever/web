<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModFeatureTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mod_feature_types', function (Blueprint $table) {
            $table->id();
            $table->string('icon');
            $table->string('name');
        });

        Schema::table('mod_features', function (Blueprint $table) {
            $table->foreignId('mod_feature_type_id')->constrained()->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mod_feature_types');
    }
}
