<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mod_files', function (Blueprint $table) {
            $table->id();
            $table->string('nameFile');
            $table->string('type')->comment("Type de fichier: image,video,text,zip");
            $table->timestamps();

            $table->foreignId('mod_id')->constrained()->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mod_files');
    }
}
