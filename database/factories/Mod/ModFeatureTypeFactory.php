<?php

namespace Database\Factories\Mod;

use App\Models\Mod\ModFeatureType;
use Illuminate\Database\Eloquent\Factories\Factory;

class ModFeatureTypeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ModFeatureType::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
