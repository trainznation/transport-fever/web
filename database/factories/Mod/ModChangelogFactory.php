<?php

namespace Database\Factories\Mod;

use App\Models\Mod\ModChangelog;
use Illuminate\Database\Eloquent\Factories\Factory;

class ModChangelogFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ModChangelog::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "version" => rand(0,9).".".rand(0,9),
            "content" => $this->faker->realText(rand(200,1900)),
        ];
    }
}
