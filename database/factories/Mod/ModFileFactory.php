<?php

namespace Database\Factories\Mod;

use App\Models\Mod\ModFile;
use Illuminate\Database\Eloquent\Factories\Factory;

class ModFileFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ModFile::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
