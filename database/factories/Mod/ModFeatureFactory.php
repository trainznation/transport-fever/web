<?php

namespace Database\Factories\Mod;

use App\Models\Mod\ModFeature;
use Illuminate\Database\Eloquent\Factories\Factory;

class ModFeatureFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ModFeature::class;

    /**
     * Define the model's default state.
     *
     * @return void
     */
    public function definition()
    {
    }
}
