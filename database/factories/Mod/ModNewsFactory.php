<?php

namespace Database\Factories\Mod;

use App\Models\Mod\ModNews;
use Illuminate\Database\Eloquent\Factories\Factory;

class ModNewsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ModNews::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $state = rand(0,2);
        return [
            "title" => $this->faker->text(),
            "slug" => $this->faker->slug(),
            "content" => $this->faker->realText(rand(300,1900)),
            "state" => $state,
            "published_at" => (($state == 0) ? null : ($state == 1)) ? now() : (($state == 2) ? now()->subDays(rand(10,365)) : null),
        ];
    }
}
