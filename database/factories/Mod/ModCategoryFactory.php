<?php

namespace Database\Factories\Mod;

use App\Models\Mod\ModCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

class ModCategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ModCategory::class;

    /**
     * Define the model's default state.
     *
     * @return void
     */
    public function definition()
    {
    }
}
