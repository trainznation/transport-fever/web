<?php

namespace Database\Factories\Mod;

use App\Models\Mod\Mod;
use Illuminate\Database\Eloquent\Factories\Factory;

class ModFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Mod::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $published = rand(0,2);
        return [
            "name" => $this->faker->text,
            "mod_id" => "trainznation_".$this->faker->word."_".rand(1,9),
            "mod_version" => rand(0,9).".".rand(0,9),
            "state" => rand(0,2),
            "mod_size" => rand(100000000,99999999),
            "description" => $this->faker->realText(rand(200,1900)),
            "published" => $published,
            "published_at" => (($published == 0) ? null : ($published == 1)) ? now() : (($published == 2) ? now()->subDays(rand(10,365)) : null),
        ];
    }
}
