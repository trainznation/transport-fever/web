<?php

namespace Database\Factories\Setting;

use App\Models\Setting\Announcement;
use Illuminate\Database\Eloquent\Factories\Factory;

class AnnouncementFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Announcement::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "title" => $this->faker->text,
            "type" => "info",
            "content" => $this->faker->text,
            "expiring_at" => now()->addDays(rand(0,250))
        ];
    }
}
