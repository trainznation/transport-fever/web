<?php

namespace Database\Factories\Blog;

use App\Models\Blog\Blog;
use Illuminate\Database\Eloquent\Factories\Factory;

class BlogFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Blog::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $state = rand(0,2);
        return [
            "title" => $this->faker->text,
            "slug" => $this->faker->slug,
            "content" => $this->faker->realText(),
            "blog_category_id" => rand(1,3),
            "state" => $state,
            "published_at" => (($state == 0) ? null : ($state == 1)) ? now() : (($state == 2) ? now()->subDays(rand(10,365)) : null),
        ];
    }
}
