<?php

namespace Database\Seeders;

use App\Models\Mod\ModCategory;
use Illuminate\Database\Seeder;

class ModCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ModCategory::create(["name" => "Ponts"]);
        ModCategory::create(["name" => "Constructions"]);
        ModCategory::create(["name" => "Bus"]);
        ModCategory::create(["name" => "Voiture"]);
        ModCategory::create(["name" => "Locomotive"]);
        ModCategory::create(["name" => "Map"]);
        ModCategory::create(["name" => "Unité Multiple"]);
        ModCategory::create(["name" => "Avions"]);
        ModCategory::create(["name" => "Passage à Niveau"]);
        ModCategory::create(["name" => "Script Mod"]);
        ModCategory::create(["name" => "Bateau"]);
        ModCategory::create(["name" => "Gares & Stations"]);
        ModCategory::create(["name" => "Routes & Rues"]);
        ModCategory::create(["name" => "Tracks"]);
        ModCategory::create(["name" => "Trams"]);
        ModCategory::create(["name" => "Camions"]);
        ModCategory::create(["name" => "Tunnels"]);
        ModCategory::create(["name" => "Wagons"]);
        ModCategory::create(["name" => "Autres"]);
    }
}
