<?php

namespace Database\Seeders;

use App\Models\Mod\Mod;
use App\Models\Mod\ModChangelog;
use Illuminate\Database\Seeder;

class ModChangelogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (env("APP_ENV") == 'local') {
            $mods = Mod::all();

            foreach ($mods as $mod) {
                ModChangelog::factory()
                    ->count(rand(1,5))
                    ->create([
                        "mod_id" => $mod->id
                    ]);
            }
        }
    }
}
