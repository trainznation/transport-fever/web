<?php

namespace Database\Seeders;

use App\Models\Mod\Mod;
use Illuminate\Database\Seeder;

class ModSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (env("APP_ENV") == 'local') {
            for($i=1; $i < 18; $i++) {
                Mod::factory()
                    ->count(5)
                    ->create([
                        "mod_category_id" => $i
                    ]);
            }
        }
    }
}
