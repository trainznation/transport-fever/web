<?php

namespace Database\Seeders;

use App\Models\Blog\BlogCategory;
use Illuminate\Database\Seeder;

class BlogCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BlogCategory::create(["name" => "Transport Fever 2"]);
        BlogCategory::create(["name" => "Trainznation Studio"]);
        BlogCategory::create(["name" => "Développement"]);
    }
}
