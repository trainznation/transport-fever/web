<?php

namespace Database\Seeders;

use App\Models\Mod\Mod;
use App\Models\Mod\ModNews;
use Illuminate\Database\Seeder;

class ModNewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(env("APP_ENV") == 'local') {
            $mods = Mod::all();

            foreach ($mods as $mod) {
                ModNews::factory()
                    ->count(rand(1,9))
                    ->create([
                        "mod_id" => $mod->id
                    ]);
            }
        }
    }
}
