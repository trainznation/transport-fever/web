<?php

namespace Database\Seeders;

use App\Models\Blog\Blog;
use Illuminate\Database\Seeder;


class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(env('APP_ENV') == 'local') {
            Blog::factory()
                ->count(rand(5,55))
                ->create();
        }
    }
}
