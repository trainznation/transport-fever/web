<?php

namespace Database\Seeders;

use App\Models\Setting\Announcement;
use Illuminate\Database\Seeder;

class AnnouncementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(env("APP_ENV") == 'local') {
            Announcement::factory()
                ->count(rand(1,5))
                ->create();
        }
    }
}
