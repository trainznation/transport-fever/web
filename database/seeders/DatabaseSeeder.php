<?php

namespace Database\Seeders;

use App\Models\Mod\ModFeatureType;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(AnnouncementSeeder::class);
        $this->call(BlogCategorySeeder::class);
        $this->call(BlogSeeder::class);
        $this->call(ModCategorySeeder::class);
        $this->call(ModSeeder::class);
        $this->call(ModFeatureTypeSeeder::class);
        $this->call(ModNewsSeeder::class);
        $this->call(ModChangelogSeeder::class);
    }
}
