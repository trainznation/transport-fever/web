<?php

namespace Database\Seeders;

use App\Models\Mod\ModFeatureType;
use Illuminate\Database\Seeder;

class ModFeatureTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ModFeatureType::create(["icon" => "openmoji:motor", "name" => "Type de Vehicule"]);
        ModFeatureType::create(["icon" => "subway:power", "name" => "Type de Carburant"]);
        ModFeatureType::create(["icon" => "fluent:top-speed-20-regular", "name" => "Vitesse"]);
        ModFeatureType::create(["icon" => "gg:performance", "name" => "Performance"]);
        ModFeatureType::create(["icon" => "mdi:car-traction-control", "name" => "Traction"]);
        ModFeatureType::create(["icon" => "bi:calendar-range", "name" => "Disponibilité"]);
        ModFeatureType::create(["icon" => "bi:flag", "name" => "Pays"]);
        ModFeatureType::create(["icon" => "mdi:human-capacity-increase", "name" => "Capacité"]);
    }
}
