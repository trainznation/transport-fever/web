<?php

use App\Http\Controllers\Api\Blog\BlogCategoryController;
use App\Http\Controllers\Api\Blog\BlogController;
use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "blog"], function() {
    Route::group(["prefix" => "post"], function() {
        Route::get('/', [BlogController::class, 'index']);
        Route::post('filter', [BlogController::class, 'listing']);
        Route::post('/', [BlogController::class, 'store']);
        Route::get('{post}', [BlogController::class, 'show']);
        Route::put('{post}', [BlogController::class, 'update']);
        Route::delete('{post}', [BlogController::class, 'destroy']);
    });

    Route::group(["prefix" => "category"], function() {
        Route::get('/', [BlogCategoryController::class, 'index']);
        Route::post('/', [BlogCategoryController::class, 'store']);
        Route::get('{category}', [BlogCategoryController::class, 'show']);
        Route::put('{category}', [BlogCategoryController::class, 'update']);
        Route::delete('{category}', [BlogCategoryController::class, 'destroy']);
    });
});
