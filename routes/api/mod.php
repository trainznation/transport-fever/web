<?php

use App\Http\Controllers\Api\Mod\ModCategoryController;
use App\Http\Controllers\Api\Mod\ModController;
use App\Http\Controllers\Api\Mod\ModFileController;
use App\Http\Controllers\Api\Mod\ModNewsController;
use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "mod"], function () {
    Route::group(["prefix" => "category"], function () {
        Route::get('/', [ModCategoryController::class, 'index']);
        Route::post('/', [ModCategoryController::class, 'store']);
        Route::get('{id}', [ModCategoryController::class, 'show']);
        Route::put('{id}', [ModCategoryController::class, 'update']);
        Route::delete('{id}', [ModCategoryController::class, 'destroy']);
    });

    Route::group(["prefix" => "mod"], function() {
        Route::get('/', [ModController::class, 'index']);
        Route::post('filter', [ModController::class, 'listing']);
        Route::post('/', [ModController::class, 'store']);
        Route::get('{id}', [ModController::class, 'show']);
        Route::put('{id}', [ModController::class, 'update']);
        Route::delete('{id}', [ModController::class, 'destroy']);
    });

    // Mod News
    Route::group(["prefix" => "mod/{modid}/news"], function () {
        Route::get('/', [ModNewsController::class, 'index']);
        Route::post('filter', [ModNewsController::class, 'listing']);
        Route::post('/', [ModNewsController::class, 'store']);
        Route::get('{id}', [ModNewsController::class, 'show']);
        Route::put('{id}', [ModNewsController::class, 'update']);
        Route::delete('{id}', [ModNewsController::class, 'destroy']);
    });

    // Mod Files
    Route::group(["prefix" => "mod/{modid}/files"], function () {
        Route::get('/', [ModFileController::class, 'index']);
        Route::post('filter', [ModFileController::class, 'listing']);
        Route::post('/', [ModFileController::class, 'store']);
        Route::get('{id}', [ModFileController::class, 'show']);
        Route::put('{id}', [ModFileController::class, 'update']);
        Route::delete('{id}', [ModFileController::class, 'destroy']);
    });
});
