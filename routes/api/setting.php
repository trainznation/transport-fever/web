<?php

use App\Http\Controllers\Api\Setting\AnnouncementController;
use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "setting"], function() {
    Route::group(["prefix" => "announcement"], function () {
        Route::get('/', [AnnouncementController::class, 'index']);
        Route::post('/', [AnnouncementController::class, 'store']);
        Route::get('{announce}', [AnnouncementController::class, 'show']);
        Route::delete('{announce}', [AnnouncementController::class, 'destroy']);
    });
});
