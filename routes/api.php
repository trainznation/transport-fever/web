<?php

use App\Http\Controllers\Api\Blog\BlogCategoryController;
use App\Http\Controllers\Api\Blog\BlogController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function () {
    return api()->response(200, "Connexion OK");
});

include("api/blog.php");
include("api/setting.php");
include("api/mod.php");
