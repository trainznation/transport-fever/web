<?php
use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "insight"], function () {
    Route::get('/', [\App\Http\Controllers\InsightController::class, 'index']);
});
