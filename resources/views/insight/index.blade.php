@extends("insight.layouts.app")

@section('content')
    <div class="container-fluid">
        <div class="card mb-3">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3">
                        {!! $code->container() !!}
                    </div>
                    <div class="col-md-3">
                        {!! $complexity->container() !!}
                    </div>
                    <div class="col-md-3">
                        {!! $archi->container() !!}
                    </div>
                    <div class="col-md-3">
                        {!! $style->container() !!}
                    </div>
                </div>
            </div>
        </div>
        <div id="accordion mb-3">
            <div class="card">
                <div class="card-header" id="cardCode">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#code" aria-expanded="true">
                            Info Code ({{ count((array) $array->code) }})
                        </button>
                    </h5>
                </div>
                <div id="code" class="collapse">
                    <div class="card-body">
                        @foreach($array->code as $item)
                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#{{ $item['stringify'] }}" aria-expanded="true" aria-controls="collapseOne">
                                                {{ $item['title'] }}
                                            </button>
                                        </h5>
                                    </div>

                                    <div id="{{ $item['stringify'] }}" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            <table class="table table-bordered table-striped">
                                                <tbody>
                                                <tr>
                                                    <td><strong>Class Insight</strong></td>
                                                    <td>{{ $item['insightClass'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Fichier</strong></td>
                                                    <td>{{ $item['file'] }} {{ isset($item['line']) ? '(Ligne '.$item['line'].')' : '' }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Message</strong></td>
                                                    <td>{{ $item['message'] }}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div id="accordion mb-3">
            <div class="card">
                <div class="card-header" id="cardComplexity">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#complexity" aria-expanded="true">
                            Info Complexity ({{ count((array) $array->complexity) }})
                        </button>
                    </h5>
                </div>
                <div id="complexity" class="collapse">
                    <div class="card-body">
                        @foreach($array->complexity as $item)
                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#{{ $item['stringify'] }}" aria-expanded="true" aria-controls="collapseOne">
                                                {{ $item['title'] }}
                                            </button>
                                        </h5>
                                    </div>

                                    <div id="{{ $item['stringify'] }}" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            <table class="table table-bordered table-striped">
                                                <tbody>
                                                <tr>
                                                    <td><strong>Class Insight</strong></td>
                                                    <td>{{ $item['insightClass'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Fichier</strong></td>
                                                    <td>{{ $item['file'] }} {{ isset($item['line']) ? '(Ligne '.$item['line'].')' : '' }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Message</strong></td>
                                                    <td>{{ $item['message'] }}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div id="accordion mb-3">
            <div class="card">
                <div class="card-header" id="cardArchitecture">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#architecture" aria-expanded="true">
                            Info Architecture ({{ count((array) $array->architecture) }})
                        </button>
                    </h5>
                </div>
                <div id="architecture" class="collapse">
                    <div class="card-body">
                        @foreach($array->architecture as $item)
                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#{{ $item['stringify'] }}" aria-expanded="true" aria-controls="collapseOne">
                                                {{ $item['title'] }}
                                            </button>
                                        </h5>
                                    </div>

                                    <div id="{{ $item['stringify'] }}" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            <table class="table table-bordered table-striped">
                                                <tbody>
                                                <tr>
                                                    <td><strong>Class Insight</strong></td>
                                                    <td>{{ $item['insightClass'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Fichier</strong></td>
                                                    <td>{{ $item['file'] }} {{ isset($item['line']) ? '(Ligne '.$item['line'].')' : '' }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Message</strong></td>
                                                    <td>{{ $item['message'] }}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div id="accordion mb-3">
            <div class="card">
                <div class="card-header" id="cardStyle">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#style" aria-expanded="true">
                            Info Style ({{ count((array) $array->style) }})
                        </button>
                    </h5>
                </div>
                <div id="style" class="collapse">
                    <div class="card-body">
                        @foreach($array->style as $item)
                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#{{ $item['stringify'] }}" aria-expanded="true" aria-controls="collapseOne">
                                                {{ $item['title'] }}
                                            </button>
                                        </h5>
                                    </div>

                                    <div id="{{ $item['stringify'] }}" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            <table class="table table-bordered table-striped">
                                                <tbody>
                                                <tr>
                                                    <td><strong>Class Insight</strong></td>
                                                    <td>{{ $item['insightClass'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Fichier</strong></td>
                                                    <td>{{ $item['file'] }} {{ isset($item['line']) ? '(Ligne '.$item['line'].')' : '' }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Message</strong></td>
                                                    <td>{{ $item['message'] }}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
