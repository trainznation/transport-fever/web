# Blog Management

Gestion du Blog

## List Post


Affiche la liste des articles

> Example request:

```bash
curl -X GET \
    -G "http://tf2.trainznation.io/api/blog/post" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://tf2.trainznation.io/api/blog/post"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://tf2.trainznation.io/api/blog/post',
    [
        'headers' => [
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "STATUS": "200",
    "MESSAGE": "List All Post",
    "DATA": [
        {
            "id": 1,
            "title": "Ut et sint deserunt ex sequi earum minima molestiae. Dolorem ut sint error pariatur architecto. Accusantium maxime itaque error molestiae maxime enim. Repudiandae cupiditate excepturi dolor numquam.",
            "slug": "quo-a-cumque-qui-natus-delectus-aliquid-et-omnis",
            "short": "Mais l'apothicaire certifia qu'il le fît au moins pour acheter des médicaments que pour les chevaux poussaient devant eux des pommes de terre. D'année...",
            "content": "Mais l'apothicaire certifia qu'il le fît au moins pour acheter des médicaments que pour les chevaux poussaient devant eux des pommes de terre. D'année en année, cependant, son petit champ se.",
            "state": 0,
            "published": false,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 3,
                "name": "Développement"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 2,
            "title": "Eos est ipsa molestias sunt voluptate est dolorem. Ex et laborum commodi et labore. Quia quibusdam minima quas sit.",
            "slug": "rerum-harum-voluptas-consequatur",
            "short": "Grand-Pont, traversa la place du bonheur dont elle attendait la preuve. Elle persista pourtant, et, lorsque le pharmacien déblatérer contre les pavés,...",
            "content": "Grand-Pont, traversa la place du bonheur dont elle attendait la preuve. Elle persista pourtant, et, lorsque le pharmacien déblatérer contre les pavés, accusa sa femme était un grand bel homme, à.",
            "state": 0,
            "published": false,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 1,
                "name": "Transport Fever 2"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 3,
            "title": "Tempore voluptas hic voluptatum quia officiis dolorem suscipit. Aspernatur illum dolorum quis quis voluptatum sunt excepturi. Delectus natus autem facere ea.",
            "slug": "delectus-quaerat-tenetur-sed-deserunt",
            "short": "Charles offrit le sien. -- Ah! des plaisanteries! Assez, assez! Faites, par pitié, que je suis pharmacien, c'est-à-dire chimiste! et la voix traînante...",
            "content": "Charles offrit le sien. -- Ah! des plaisanteries! Assez, assez! Faites, par pitié, que je suis pharmacien, c'est-à-dire chimiste! et la voix traînante, et portant, ce soir-là, sur ses lèvres sur le.",
            "state": 1,
            "published": 1616598900,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 1,
                "name": "Transport Fever 2"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 4,
            "title": "Id non impedit id facilis quae dignissimos. Ut voluptas a non dolores molestias dolorem. Iure sequi rerum veniam. Consequuntur iste quidem aspernatur officia occaecati.",
            "slug": "dignissimos-quo-non-quo-non-debitis-amet-cum",
            "short": "Allez! allez! monsieur Homais, ils ont peut-être fait quinze parties et bu huit pots de cidre!... Mais ils vont me déchirer le tapis, et Lheureux, aff...",
            "content": "Allez! allez! monsieur Homais, ils ont peut-être fait quinze parties et bu huit pots de cidre!... Mais ils vont me déchirer le tapis, et Lheureux, affirmant «que ce n'était pas si elle embrassait la.",
            "state": 2,
            "published": 1597414500,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 1,
                "name": "Transport Fever 2"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 5,
            "title": "Quas dolores ea amet ratione sed pariatur facere. Ad consectetur nihil saepe veniam culpa. At eum nobis voluptatem odit dolor sit ex odit.",
            "slug": "magnam-qui-sint-doloremque-veniam-enim",
            "short": "Restauration, le Marquis, cherchant à cacheter la lettre, le cachet _Amor nel cor_; de plus, et Théodore, le domestique du notaire, qui devint tout à...",
            "content": "Restauration, le Marquis, cherchant à cacheter la lettre, le cachet _Amor nel cor_; de plus, et Théodore, le domestique du notaire, qui devint tout à l'heure pour si peu de chose; il en convenait.",
            "state": 2,
            "published": 1608995700,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 2,
                "name": "Trainznation Studio"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 6,
            "title": "Quo eos et quaerat rerum porro. Rerum at eveniet omnis. Nihil nisi possimus est quas facilis.",
            "slug": "placeat-qui-velit-culpa-voluptas-amet-repellendus",
            "short": "Il se traînait à genoux vers elle, se déclarant envoyé par Edgar. Il avouait, du reste, de comprendre même la plus vague, l'abandonna. Mais, par un ma...",
            "content": "Il se traînait à genoux vers elle, se déclarant envoyé par Edgar. Il avouait, du reste, de comprendre même la plus vague, l'abandonna. Mais, par un matin d'automne, flottait au-dessus de sa trop.",
            "state": 2,
            "published": 1607613300,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 1,
                "name": "Transport Fever 2"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 7,
            "title": "Corporis illum debitis quibusdam itaque suscipit. Et et voluptatem rerum labore animi quaerat. Rerum odit distinctio nemo repellat odit ut in. Eligendi recusandae quia hic numquam sed voluptatum.",
            "slug": "temporibus-laborum-labore-minus",
            "short": "Saint-Victor et fuma jusqu'au jour, tout en buvant des grogs au kirsch, mélange inconnu à la persienne un petit salon, des danseurs hauts comme le vis...",
            "content": "Saint-Victor et fuma jusqu'au jour, tout en buvant des grogs au kirsch, mélange inconnu à la persienne un petit salon, des danseurs hauts comme le visage comme quelqu'un de ruiné qui regarde, à.",
            "state": 1,
            "published": 1616598900,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 1,
                "name": "Transport Fever 2"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 8,
            "title": "Odio sit totam itaque repellendus ut fugiat. Temporibus qui voluptatem perferendis itaque voluptates. Voluptas eos rem ipsum alias distinctio fuga non expedita.",
            "slug": "aut-eligendi-quam-ipsa-illum",
            "short": "Léon! Elle rit, pleura, chanta, dansa, fit monter des sorbets, voulut fumer des pastilles du sérail qu'elle avait trop d'esprit pour la baiser; elle s...",
            "content": "Léon! Elle rit, pleura, chanta, dansa, fit monter des sorbets, voulut fumer des pastilles du sérail qu'elle avait trop d'esprit pour la baiser; elle se dégagea de la prairie. Au bout de ses lèvres.",
            "state": 2,
            "published": 1607440500,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 1,
                "name": "Transport Fever 2"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 9,
            "title": "Sed sint et est sint. Possimus earum eligendi et vero ipsa aut rerum natus. Iusto assumenda mollitia corporis vel aut. Inventore ut quia ullam eum deserunt.",
            "slug": "quia-quis-debitis-magni-laborum-aliquam",
            "short": "Ils s'en revenaient à la peau. Elle sentait dans sa maison. -- Et que va-t-il arriver, maintenant? reprit-elle. -- Quel misérable! quel goujat!... que...",
            "content": "Ils s'en revenaient à la peau. Elle sentait dans sa maison. -- Et que va-t-il arriver, maintenant? reprit-elle. -- Quel misérable! quel goujat!... quelle infamie! se disait-elle, en fuyant d'un pied.",
            "state": 0,
            "published": false,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 3,
                "name": "Développement"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 10,
            "title": "Tempore architecto quibusdam natus sed illo. Et aspernatur ipsum aperiam quasi voluptatum illum. Minus aspernatur quas eius eveniet quo labore exercitationem sed.",
            "slug": "ex-aut-et-alias-reiciendis-vero-quis",
            "short": "Paris, titres de romans, quadrilles nouveaux, et le faubourg, jusqu'à une rue découverte qui dominait celui des voix, le jappement des chiens sur le h...",
            "content": "Paris, titres de romans, quadrilles nouveaux, et le faubourg, jusqu'à une rue découverte qui dominait celui des voix, le jappement des chiens sur le haut par une corde. Cependant, entre les draps.",
            "state": 1,
            "published": 1616598900,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 1,
                "name": "Transport Fever 2"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 11,
            "title": "Nihil velit suscipit sint ut atque. Et assumenda fugit perspiciatis et. Consequatur maiores tempore ut. Dolor ut tempora minima magnam.",
            "slug": "eum-ex-nostrum-sunt-laudantium-et-error",
            "short": "Elle descendait toujours. Enfin on entendit un choc; les cordes grinçassent mieux, et puis la garda sur son tour, dont le ronflement d'une toupie bour...",
            "content": "Elle descendait toujours. Enfin on entendit un choc; les cordes grinçassent mieux, et puis la garda sur son tour, dont le ronflement d'une toupie bourdonnait à ses narines, le coeur battant. Emma.",
            "state": 1,
            "published": 1616598900,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 3,
                "name": "Développement"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 12,
            "title": "Aut explicabo veniam et sapiente distinctio distinctio eaque ipsum. Reprehenderit quia ut voluptatum quia et veniam maxime.",
            "slug": "ut-quibusdam-aliquam-quam-ab-impedit-delectus-voluptate-velit",
            "short": "Mais, si je vous recommande, il entre en cinquième. Si son travail et sa robe trop longue aussi, et sans détourner la tête. II En arrivant à la maison...",
            "content": "Mais, si je vous recommande, il entre en cinquième. Si son travail et sa robe trop longue aussi, et sans détourner la tête. II En arrivant à la maison pour fêter son succès. Il partit à Neufchâtel.",
            "state": 0,
            "published": false,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 1,
                "name": "Transport Fever 2"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 13,
            "title": "Ut accusantium harum sapiente. Non aperiam ullam ab quo. Ut quos eos nulla et ut quibusdam. Facilis quis et necessitatibus ratione ullam dolores.",
            "slug": "voluptatem-aut-odio-aut",
            "short": "Rodolphe, peut-être, au point du billet. Elle n'y songeait pas; Charles, au début d'une chanteuse, à l'ouverture d'un magasin. Elle savait valser, cel...",
            "content": "Rodolphe, peut-être, au point du billet. Elle n'y songeait pas; Charles, au début d'une chanteuse, à l'ouverture d'un magasin. Elle savait valser, celle-là! Ils continuèrent longtemps et fatiguèrent.",
            "state": 1,
            "published": 1616598900,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 3,
                "name": "Développement"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 14,
            "title": "Et eveniet provident numquam doloremque nam. Ad dicta sed ut dolorem velit debitis et. Dolorem illum odio eveniet rem iure aliquam soluta. Molestiae tenetur earum possimus aliquid enim qui.",
            "slug": "consequatur-adipisci-dolorem-eos-error-velit",
            "short": "J'aurais dû m'épargner cette dernière honte. Tu ne les comprit pas; ensuite, par des civilités, l'autre s'avoua confus; et ils restèrent assis l'un en...",
            "content": "J'aurais dû m'épargner cette dernière honte. Tu ne les comprit pas; ensuite, par des civilités, l'autre s'avoua confus; et ils restèrent assis l'un en face d'elle, au pied du lit, tandis qu'elle.",
            "state": 2,
            "published": 1598192100,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 1,
                "name": "Transport Fever 2"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 15,
            "title": "Ut libero perferendis dolores. Voluptas nemo et incidunt tenetur numquam. Sit ducimus laboriosam non expedita. Ut voluptatum veniam modi cum eveniet omnis. Eos beatae illum totam est vitae.",
            "slug": "tempore-doloribus-sapiente-at-repellat-iste-reiciendis",
            "short": "Ils rencontrèrent en bas, tous les autres et s'y mettaient eux-mêmes. Suivant leur position de quelqu'un comme une flèche d'or un dernier coup d'oeil,...",
            "content": "Ils rencontrèrent en bas, tous les autres et s'y mettaient eux-mêmes. Suivant leur position de quelqu'un comme une flèche d'or un dernier coup d'oeil, la ville arrivaient à la tombée du jour.",
            "state": 0,
            "published": false,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 1,
                "name": "Transport Fever 2"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 16,
            "title": "Ea omnis ab sunt sed eos vitae. Veniam ut esse voluptatibus aut perferendis quas. Magni in saepe voluptates debitis. Quis nihil aut eos et porro. Accusamus facilis beatae quo iure laboriosam.",
            "slug": "ducimus-et-distinctio-hic-id-est",
            "short": "Cependant, il y avait de grands yeux tristes et se mit à fuir vers sa femme et de poils de lapin; venait ensuite une pyramide, puis un temple avec por...",
            "content": "Cependant, il y avait de grands yeux tristes et se mit à fuir vers sa femme et de poils de lapin; venait ensuite une pyramide, puis un temple avec portiques, colonnades et statuettes de stuc tout.",
            "state": 1,
            "published": 1616598900,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 2,
                "name": "Trainznation Studio"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 17,
            "title": "Enim a quia ad velit. Est expedita omnis quia et omnis temporibus. Ut dolores et atque odit excepturi.",
            "slug": "velit-illo-adipisci-deserunt-nihil",
            "short": "Mais, la maison du notaire, et il faisait depuis douze heures, il entra majestueusement sans retirer son chapeau, d'un mouvement plus rapide, le vicom...",
            "content": "Mais, la maison du notaire, et il faisait depuis douze heures, il entra majestueusement sans retirer son chapeau, d'un mouvement plus rapide, le vicomte, l'entraînant, disparut avec elle des.",
            "state": 0,
            "published": false,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 2,
                "name": "Trainznation Studio"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 18,
            "title": "Nostrum esse deserunt debitis id. Rerum eos odio nulla tempora repudiandae voluptatem velit. Earum debitis voluptatem et quidem. Rerum quidem id deserunt cupiditate pariatur libero.",
            "slug": "maxime-iste-voluptatem-assumenda-recusandae-ut-neque",
            "short": "Il vient de recevoir la récompense de vos messieurs? demanda le clerc. Cela se passera, jugea-t-il, c'est un bouquet de mariage. Les boutons d'oranger...",
            "content": "Il vient de recevoir la récompense de vos messieurs? demanda le clerc. Cela se passera, jugea-t-il, c'est un bouquet de mariage. Les boutons d'oranger étaient jaunes de poussière, et les chasseurs.",
            "state": 1,
            "published": 1616598900,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 3,
                "name": "Développement"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 19,
            "title": "Non rerum necessitatibus sed sit ipsa. Rerum sequi facere maiores minus dolorem laboriosam. Deleniti quia et quidem. Consequatur et modi quibusdam quibusdam mollitia odit a.",
            "slug": "ad-totam-accusamus-mollitia-id-doloremque-inventore-aliquid",
            "short": "Cependant les quatre couverts. Homais demanda la maîtresse d'auberge, tout en lui passant la main sur le journal en les respirant, se gonfla d'orgueil...",
            "content": "Cependant les quatre couverts. Homais demanda la maîtresse d'auberge, tout en lui passant la main sur le journal en les respirant, se gonfla d'orgueil, comme si cet hommage qu'il destinait à une.",
            "state": 2,
            "published": 1598019300,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 3,
                "name": "Développement"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 20,
            "title": "Quod magni eos doloribus. Veritatis sunt minima excepturi harum voluptas. Est maxime impedit aut voluptatem et magni.",
            "slug": "consequatur-et-quia-aliquid-ex-ad",
            "short": "M. Lieuvain, au monarque! M. Tuvache, le maire, avec ses gros sabots; sa blouse avait des trous, ses pieds était plus blême et plus pâle et plus massi...",
            "content": "M. Lieuvain, au monarque! M. Tuvache, le maire, avec ses gros sabots; sa blouse avait des trous, ses pieds était plus blême et plus pâle et plus massif. Mais, la maison du pharmacien. Elle a, au.",
            "state": 0,
            "published": false,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 1,
                "name": "Transport Fever 2"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 21,
            "title": "Sit asperiores quia nihil alias ratione. Molestias est in esse. Harum quam magnam tempora veniam et distinctio laudantium reprehenderit. Rerum autem placeat debitis ducimus qui.",
            "slug": "impedit-incidunt-fuga-iure-sapiente-dolores-nobis",
            "short": "Sur la ligne courbe du crâne; et, laissant voir à Bovary): c'était une surprise sentimentale qu'il réservait à sa faiblesse les apparences flatteuses...",
            "content": "Sur la ligne courbe du crâne; et, laissant voir à Bovary): c'était une surprise sentimentale qu'il réservait à sa faiblesse les apparences flatteuses d'une préoccupation supérieure. Quel.",
            "state": 0,
            "published": false,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 3,
                "name": "Développement"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 22,
            "title": "Voluptatem aut quod nesciunt est corrupti eius. Veniam veritatis accusamus placeat deleniti velit tempore. Sint quam qui odio culpa est aspernatur. Magni mollitia aliquam illo voluptatem ex veniam.",
            "slug": "consequatur-dignissimos-eligendi-quam-et-eum-debitis",
            "short": "Emma eût, au contraire, clabauder! Cela se répandrait jusqu'à Forges! jusqu'à Neufchâtel! jusqu'à Rouen! partout! Qui sait si des pièces d'or, s'évent...",
            "content": "Emma eût, au contraire, clabauder! Cela se répandrait jusqu'à Forges! jusqu'à Neufchâtel! jusqu'à Rouen! partout! Qui sait si des pièces d'or, s'éventrant de leurs doigts se confondirent.",
            "state": 2,
            "published": 1593267300,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 1,
                "name": "Transport Fever 2"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        }
    ]
}
```
<div id="execution-results-GETapi-blog-post" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-blog-post"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-blog-post"></code></pre>
</div>
<div id="execution-error-GETapi-blog-post" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-blog-post"></code></pre>
</div>
<form id="form-GETapi-blog-post" data-method="GET" data-path="api/blog/post" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-blog-post', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-blog-post" onclick="tryItOut('GETapi-blog-post');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-blog-post" onclick="cancelTryOut('GETapi-blog-post');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-blog-post" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/blog/post</code></b>
</p>
</form>


## Listing Filter Article




> Example request:

```bash
curl -X POST \
    "http://tf2.trainznation.io/api/blog/post/filter" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"$category":1,"$order":"asc","$limit":5,"$published":1}'

```

```javascript
const url = new URL(
    "http://tf2.trainznation.io/api/blog/post/filter"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "$category": 1,
    "$order": "asc",
    "$limit": 5,
    "$published": 1
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://tf2.trainznation.io/api/blog/post/filter',
    [
        'headers' => [
            'Accept' => 'application/json',
        ],
        'json' => [
            '$category' => 1,
            '$order' => 'asc',
            '$limit' => 5,
            '$published' => 1,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "STATUS": "200",
    "MESSAGE": "List Post",
    "DATA": [
        {
            "id": 1,
            "title": "Ut et sint deserunt ex sequi earum minima molestiae. Dolorem ut sint error pariatur architecto. Accusantium maxime itaque error molestiae maxime enim. Repudiandae cupiditate excepturi dolor numquam.",
            "slug": "quo-a-cumque-qui-natus-delectus-aliquid-et-omnis",
            "short": "Mais l'apothicaire certifia qu'il le fît au moins pour acheter des médicaments que pour les chevaux poussaient devant eux des pommes de terre. D'année...",
            "content": "Mais l'apothicaire certifia qu'il le fît au moins pour acheter des médicaments que pour les chevaux poussaient devant eux des pommes de terre. D'année en année, cependant, son petit champ se.",
            "state": 0,
            "published": false,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 3,
                "name": "Développement"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 2,
            "title": "Eos est ipsa molestias sunt voluptate est dolorem. Ex et laborum commodi et labore. Quia quibusdam minima quas sit.",
            "slug": "rerum-harum-voluptas-consequatur",
            "short": "Grand-Pont, traversa la place du bonheur dont elle attendait la preuve. Elle persista pourtant, et, lorsque le pharmacien déblatérer contre les pavés,...",
            "content": "Grand-Pont, traversa la place du bonheur dont elle attendait la preuve. Elle persista pourtant, et, lorsque le pharmacien déblatérer contre les pavés, accusa sa femme était un grand bel homme, à.",
            "state": 0,
            "published": false,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 1,
                "name": "Transport Fever 2"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 3,
            "title": "Tempore voluptas hic voluptatum quia officiis dolorem suscipit. Aspernatur illum dolorum quis quis voluptatum sunt excepturi. Delectus natus autem facere ea.",
            "slug": "delectus-quaerat-tenetur-sed-deserunt",
            "short": "Charles offrit le sien. -- Ah! des plaisanteries! Assez, assez! Faites, par pitié, que je suis pharmacien, c'est-à-dire chimiste! et la voix traînante...",
            "content": "Charles offrit le sien. -- Ah! des plaisanteries! Assez, assez! Faites, par pitié, que je suis pharmacien, c'est-à-dire chimiste! et la voix traînante, et portant, ce soir-là, sur ses lèvres sur le.",
            "state": 1,
            "published": 1616598900,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 1,
                "name": "Transport Fever 2"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 4,
            "title": "Id non impedit id facilis quae dignissimos. Ut voluptas a non dolores molestias dolorem. Iure sequi rerum veniam. Consequuntur iste quidem aspernatur officia occaecati.",
            "slug": "dignissimos-quo-non-quo-non-debitis-amet-cum",
            "short": "Allez! allez! monsieur Homais, ils ont peut-être fait quinze parties et bu huit pots de cidre!... Mais ils vont me déchirer le tapis, et Lheureux, aff...",
            "content": "Allez! allez! monsieur Homais, ils ont peut-être fait quinze parties et bu huit pots de cidre!... Mais ils vont me déchirer le tapis, et Lheureux, affirmant «que ce n'était pas si elle embrassait la.",
            "state": 2,
            "published": 1597414500,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 1,
                "name": "Transport Fever 2"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 5,
            "title": "Quas dolores ea amet ratione sed pariatur facere. Ad consectetur nihil saepe veniam culpa. At eum nobis voluptatem odit dolor sit ex odit.",
            "slug": "magnam-qui-sint-doloremque-veniam-enim",
            "short": "Restauration, le Marquis, cherchant à cacheter la lettre, le cachet _Amor nel cor_; de plus, et Théodore, le domestique du notaire, qui devint tout à...",
            "content": "Restauration, le Marquis, cherchant à cacheter la lettre, le cachet _Amor nel cor_; de plus, et Théodore, le domestique du notaire, qui devint tout à l'heure pour si peu de chose; il en convenait.",
            "state": 2,
            "published": 1608995700,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 2,
                "name": "Trainznation Studio"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 6,
            "title": "Quo eos et quaerat rerum porro. Rerum at eveniet omnis. Nihil nisi possimus est quas facilis.",
            "slug": "placeat-qui-velit-culpa-voluptas-amet-repellendus",
            "short": "Il se traînait à genoux vers elle, se déclarant envoyé par Edgar. Il avouait, du reste, de comprendre même la plus vague, l'abandonna. Mais, par un ma...",
            "content": "Il se traînait à genoux vers elle, se déclarant envoyé par Edgar. Il avouait, du reste, de comprendre même la plus vague, l'abandonna. Mais, par un matin d'automne, flottait au-dessus de sa trop.",
            "state": 2,
            "published": 1607613300,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 1,
                "name": "Transport Fever 2"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 7,
            "title": "Corporis illum debitis quibusdam itaque suscipit. Et et voluptatem rerum labore animi quaerat. Rerum odit distinctio nemo repellat odit ut in. Eligendi recusandae quia hic numquam sed voluptatum.",
            "slug": "temporibus-laborum-labore-minus",
            "short": "Saint-Victor et fuma jusqu'au jour, tout en buvant des grogs au kirsch, mélange inconnu à la persienne un petit salon, des danseurs hauts comme le vis...",
            "content": "Saint-Victor et fuma jusqu'au jour, tout en buvant des grogs au kirsch, mélange inconnu à la persienne un petit salon, des danseurs hauts comme le visage comme quelqu'un de ruiné qui regarde, à.",
            "state": 1,
            "published": 1616598900,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 1,
                "name": "Transport Fever 2"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 8,
            "title": "Odio sit totam itaque repellendus ut fugiat. Temporibus qui voluptatem perferendis itaque voluptates. Voluptas eos rem ipsum alias distinctio fuga non expedita.",
            "slug": "aut-eligendi-quam-ipsa-illum",
            "short": "Léon! Elle rit, pleura, chanta, dansa, fit monter des sorbets, voulut fumer des pastilles du sérail qu'elle avait trop d'esprit pour la baiser; elle s...",
            "content": "Léon! Elle rit, pleura, chanta, dansa, fit monter des sorbets, voulut fumer des pastilles du sérail qu'elle avait trop d'esprit pour la baiser; elle se dégagea de la prairie. Au bout de ses lèvres.",
            "state": 2,
            "published": 1607440500,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 1,
                "name": "Transport Fever 2"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 9,
            "title": "Sed sint et est sint. Possimus earum eligendi et vero ipsa aut rerum natus. Iusto assumenda mollitia corporis vel aut. Inventore ut quia ullam eum deserunt.",
            "slug": "quia-quis-debitis-magni-laborum-aliquam",
            "short": "Ils s'en revenaient à la peau. Elle sentait dans sa maison. -- Et que va-t-il arriver, maintenant? reprit-elle. -- Quel misérable! quel goujat!... que...",
            "content": "Ils s'en revenaient à la peau. Elle sentait dans sa maison. -- Et que va-t-il arriver, maintenant? reprit-elle. -- Quel misérable! quel goujat!... quelle infamie! se disait-elle, en fuyant d'un pied.",
            "state": 0,
            "published": false,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 3,
                "name": "Développement"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 10,
            "title": "Tempore architecto quibusdam natus sed illo. Et aspernatur ipsum aperiam quasi voluptatum illum. Minus aspernatur quas eius eveniet quo labore exercitationem sed.",
            "slug": "ex-aut-et-alias-reiciendis-vero-quis",
            "short": "Paris, titres de romans, quadrilles nouveaux, et le faubourg, jusqu'à une rue découverte qui dominait celui des voix, le jappement des chiens sur le h...",
            "content": "Paris, titres de romans, quadrilles nouveaux, et le faubourg, jusqu'à une rue découverte qui dominait celui des voix, le jappement des chiens sur le haut par une corde. Cependant, entre les draps.",
            "state": 1,
            "published": 1616598900,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 1,
                "name": "Transport Fever 2"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 11,
            "title": "Nihil velit suscipit sint ut atque. Et assumenda fugit perspiciatis et. Consequatur maiores tempore ut. Dolor ut tempora minima magnam.",
            "slug": "eum-ex-nostrum-sunt-laudantium-et-error",
            "short": "Elle descendait toujours. Enfin on entendit un choc; les cordes grinçassent mieux, et puis la garda sur son tour, dont le ronflement d'une toupie bour...",
            "content": "Elle descendait toujours. Enfin on entendit un choc; les cordes grinçassent mieux, et puis la garda sur son tour, dont le ronflement d'une toupie bourdonnait à ses narines, le coeur battant. Emma.",
            "state": 1,
            "published": 1616598900,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 3,
                "name": "Développement"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 12,
            "title": "Aut explicabo veniam et sapiente distinctio distinctio eaque ipsum. Reprehenderit quia ut voluptatum quia et veniam maxime.",
            "slug": "ut-quibusdam-aliquam-quam-ab-impedit-delectus-voluptate-velit",
            "short": "Mais, si je vous recommande, il entre en cinquième. Si son travail et sa robe trop longue aussi, et sans détourner la tête. II En arrivant à la maison...",
            "content": "Mais, si je vous recommande, il entre en cinquième. Si son travail et sa robe trop longue aussi, et sans détourner la tête. II En arrivant à la maison pour fêter son succès. Il partit à Neufchâtel.",
            "state": 0,
            "published": false,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 1,
                "name": "Transport Fever 2"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 13,
            "title": "Ut accusantium harum sapiente. Non aperiam ullam ab quo. Ut quos eos nulla et ut quibusdam. Facilis quis et necessitatibus ratione ullam dolores.",
            "slug": "voluptatem-aut-odio-aut",
            "short": "Rodolphe, peut-être, au point du billet. Elle n'y songeait pas; Charles, au début d'une chanteuse, à l'ouverture d'un magasin. Elle savait valser, cel...",
            "content": "Rodolphe, peut-être, au point du billet. Elle n'y songeait pas; Charles, au début d'une chanteuse, à l'ouverture d'un magasin. Elle savait valser, celle-là! Ils continuèrent longtemps et fatiguèrent.",
            "state": 1,
            "published": 1616598900,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 3,
                "name": "Développement"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 14,
            "title": "Et eveniet provident numquam doloremque nam. Ad dicta sed ut dolorem velit debitis et. Dolorem illum odio eveniet rem iure aliquam soluta. Molestiae tenetur earum possimus aliquid enim qui.",
            "slug": "consequatur-adipisci-dolorem-eos-error-velit",
            "short": "J'aurais dû m'épargner cette dernière honte. Tu ne les comprit pas; ensuite, par des civilités, l'autre s'avoua confus; et ils restèrent assis l'un en...",
            "content": "J'aurais dû m'épargner cette dernière honte. Tu ne les comprit pas; ensuite, par des civilités, l'autre s'avoua confus; et ils restèrent assis l'un en face d'elle, au pied du lit, tandis qu'elle.",
            "state": 2,
            "published": 1598192100,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 1,
                "name": "Transport Fever 2"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 15,
            "title": "Ut libero perferendis dolores. Voluptas nemo et incidunt tenetur numquam. Sit ducimus laboriosam non expedita. Ut voluptatum veniam modi cum eveniet omnis. Eos beatae illum totam est vitae.",
            "slug": "tempore-doloribus-sapiente-at-repellat-iste-reiciendis",
            "short": "Ils rencontrèrent en bas, tous les autres et s'y mettaient eux-mêmes. Suivant leur position de quelqu'un comme une flèche d'or un dernier coup d'oeil,...",
            "content": "Ils rencontrèrent en bas, tous les autres et s'y mettaient eux-mêmes. Suivant leur position de quelqu'un comme une flèche d'or un dernier coup d'oeil, la ville arrivaient à la tombée du jour.",
            "state": 0,
            "published": false,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 1,
                "name": "Transport Fever 2"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 16,
            "title": "Ea omnis ab sunt sed eos vitae. Veniam ut esse voluptatibus aut perferendis quas. Magni in saepe voluptates debitis. Quis nihil aut eos et porro. Accusamus facilis beatae quo iure laboriosam.",
            "slug": "ducimus-et-distinctio-hic-id-est",
            "short": "Cependant, il y avait de grands yeux tristes et se mit à fuir vers sa femme et de poils de lapin; venait ensuite une pyramide, puis un temple avec por...",
            "content": "Cependant, il y avait de grands yeux tristes et se mit à fuir vers sa femme et de poils de lapin; venait ensuite une pyramide, puis un temple avec portiques, colonnades et statuettes de stuc tout.",
            "state": 1,
            "published": 1616598900,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 2,
                "name": "Trainznation Studio"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 17,
            "title": "Enim a quia ad velit. Est expedita omnis quia et omnis temporibus. Ut dolores et atque odit excepturi.",
            "slug": "velit-illo-adipisci-deserunt-nihil",
            "short": "Mais, la maison du notaire, et il faisait depuis douze heures, il entra majestueusement sans retirer son chapeau, d'un mouvement plus rapide, le vicom...",
            "content": "Mais, la maison du notaire, et il faisait depuis douze heures, il entra majestueusement sans retirer son chapeau, d'un mouvement plus rapide, le vicomte, l'entraînant, disparut avec elle des.",
            "state": 0,
            "published": false,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 2,
                "name": "Trainznation Studio"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 18,
            "title": "Nostrum esse deserunt debitis id. Rerum eos odio nulla tempora repudiandae voluptatem velit. Earum debitis voluptatem et quidem. Rerum quidem id deserunt cupiditate pariatur libero.",
            "slug": "maxime-iste-voluptatem-assumenda-recusandae-ut-neque",
            "short": "Il vient de recevoir la récompense de vos messieurs? demanda le clerc. Cela se passera, jugea-t-il, c'est un bouquet de mariage. Les boutons d'oranger...",
            "content": "Il vient de recevoir la récompense de vos messieurs? demanda le clerc. Cela se passera, jugea-t-il, c'est un bouquet de mariage. Les boutons d'oranger étaient jaunes de poussière, et les chasseurs.",
            "state": 1,
            "published": 1616598900,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 3,
                "name": "Développement"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 19,
            "title": "Non rerum necessitatibus sed sit ipsa. Rerum sequi facere maiores minus dolorem laboriosam. Deleniti quia et quidem. Consequatur et modi quibusdam quibusdam mollitia odit a.",
            "slug": "ad-totam-accusamus-mollitia-id-doloremque-inventore-aliquid",
            "short": "Cependant les quatre couverts. Homais demanda la maîtresse d'auberge, tout en lui passant la main sur le journal en les respirant, se gonfla d'orgueil...",
            "content": "Cependant les quatre couverts. Homais demanda la maîtresse d'auberge, tout en lui passant la main sur le journal en les respirant, se gonfla d'orgueil, comme si cet hommage qu'il destinait à une.",
            "state": 2,
            "published": 1598019300,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 3,
                "name": "Développement"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 20,
            "title": "Quod magni eos doloribus. Veritatis sunt minima excepturi harum voluptas. Est maxime impedit aut voluptatem et magni.",
            "slug": "consequatur-et-quia-aliquid-ex-ad",
            "short": "M. Lieuvain, au monarque! M. Tuvache, le maire, avec ses gros sabots; sa blouse avait des trous, ses pieds était plus blême et plus pâle et plus massi...",
            "content": "M. Lieuvain, au monarque! M. Tuvache, le maire, avec ses gros sabots; sa blouse avait des trous, ses pieds était plus blême et plus pâle et plus massif. Mais, la maison du pharmacien. Elle a, au.",
            "state": 0,
            "published": false,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 1,
                "name": "Transport Fever 2"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 21,
            "title": "Sit asperiores quia nihil alias ratione. Molestias est in esse. Harum quam magnam tempora veniam et distinctio laudantium reprehenderit. Rerum autem placeat debitis ducimus qui.",
            "slug": "impedit-incidunt-fuga-iure-sapiente-dolores-nobis",
            "short": "Sur la ligne courbe du crâne; et, laissant voir à Bovary): c'était une surprise sentimentale qu'il réservait à sa faiblesse les apparences flatteuses...",
            "content": "Sur la ligne courbe du crâne; et, laissant voir à Bovary): c'était une surprise sentimentale qu'il réservait à sa faiblesse les apparences flatteuses d'une préoccupation supérieure. Quel.",
            "state": 0,
            "published": false,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 3,
                "name": "Développement"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        },
        {
            "id": 22,
            "title": "Voluptatem aut quod nesciunt est corrupti eius. Veniam veritatis accusamus placeat deleniti velit tempore. Sint quam qui odio culpa est aspernatur. Magni mollitia aliquam illo voluptatem ex veniam.",
            "slug": "consequatur-dignissimos-eligendi-quam-et-eum-debitis",
            "short": "Emma eût, au contraire, clabauder! Cela se répandrait jusqu'à Forges! jusqu'à Neufchâtel! jusqu'à Rouen! partout! Qui sait si des pièces d'or, s'évent...",
            "content": "Emma eût, au contraire, clabauder! Cela se répandrait jusqu'à Forges! jusqu'à Neufchâtel! jusqu'à Rouen! partout! Qui sait si des pièces d'or, s'éventrant de leurs doigts se confondirent.",
            "state": 2,
            "published": 1593267300,
            "created": "2021-03-24T15:15:00.000000Z",
            "updated": "2021-03-24T15:15:00.000000Z",
            "category": {
                "id": 1,
                "name": "Transport Fever 2"
            },
            "image": "https:\/\/via.placeholder.com\/1920x1080"
        }
    ]
}
```
<div id="execution-results-POSTapi-blog-post-filter" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-blog-post-filter"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-blog-post-filter"></code></pre>
</div>
<div id="execution-error-POSTapi-blog-post-filter" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-blog-post-filter"></code></pre>
</div>
<form id="form-POSTapi-blog-post-filter" data-method="POST" data-path="api/blog/post/filter" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-blog-post-filter', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-blog-post-filter" onclick="tryItOut('POSTapi-blog-post-filter');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-blog-post-filter" onclick="cancelTryOut('POSTapi-blog-post-filter');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-blog-post-filter" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/blog/post/filter</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>$category</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="$category" data-endpoint="POSTapi-blog-post-filter" data-component="body"  hidden>
<br>
ID de la catégorie à rechercher.</p>
<p>
<b><code>$order</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="$order" data-endpoint="POSTapi-blog-post-filter" data-component="body"  hidden>
<br>
Ordre de la recherche.</p>
<p>
<b><code>$limit</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="$limit" data-endpoint="POSTapi-blog-post-filter" data-component="body"  hidden>
<br>
Limit à afficher de la recherche.</p>
<p>
<b><code>$published</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="$published" data-endpoint="POSTapi-blog-post-filter" data-component="body"  hidden>
<br>
Etat de la publication (0,1 ou 2).</p>

</form>


## Create Post


Création d'un article

> Example request:

```bash
curl -X POST \
    "http://tf2.trainznation.io/api/blog/post" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"title":"Ceci est un titre","content":"Ceci est la description d'un article avec beaucoups de caract\u00e8res","blog_category_id":"et"}'

```

```javascript
const url = new URL(
    "http://tf2.trainznation.io/api/blog/post"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "title": "Ceci est un titre",
    "content": "Ceci est la description d'un article avec beaucoups de caract\u00e8res",
    "blog_category_id": "et"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://tf2.trainznation.io/api/blog/post',
    [
        'headers' => [
            'Accept' => 'application/json',
        ],
        'json' => [
            'title' => 'Ceci est un titre',
            'content' => 'Ceci est la description d\'un article avec beaucoups de caractères',
            'blog_category_id' => 'et',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "STATUS": "500",
    "MESSAGE": "Erreur lors de la création du post",
    "DATA": {
        "code": "ERROR-500-2021-03-18-002",
        "error": "SQLSTATE[HY000]: General error: 1366 Incorrect integer value: 'et' for column 'blog_category_id' at row 1 (SQL: insert into `blogs` (`title`, `slug`, `content`, `state`, `published_at`, `blog_category_id`, `updated_at`, `created_at`) values (Ceci est un titre, ceci-est-un-titre, Ceci est la description d'un article avec beaucoups de caractères, 0, ?, et, 2021-03-24 17:07:16, 2021-03-24 17:07:16))",
        "trace": "#0 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(638): Illuminate\\Database\\Connection->runQueryCallback('insert into `bl...', Array, Object(Closure))\n#1 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(472): Illuminate\\Database\\Connection->run('insert into `bl...', Array, Object(Closure))\n#2 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(424): Illuminate\\Database\\Connection->statement('insert into `bl...', Array)\n#3 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Processors\\Processor.php(32): Illuminate\\Database\\Connection->insert('insert into `bl...', Array)\n#4 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(2882): Illuminate\\Database\\Query\\Processors\\Processor->processInsertGetId(Object(Illuminate\\Database\\Query\\Builder), 'insert into `bl...', Array, 'id')\n#5 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Builder.php(1547): Illuminate\\Database\\Query\\Builder->insertGetId(Array, 'id')\n#6 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Model.php(1052): Illuminate\\Database\\Eloquent\\Builder->__call('insertGetId', Array)\n#7 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Model.php(1017): Illuminate\\Database\\Eloquent\\Model->insertAndSetId(Object(Illuminate\\Database\\Eloquent\\Builder), Array)\n#8 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Model.php(858): Illuminate\\Database\\Eloquent\\Model->performInsert(Object(Illuminate\\Database\\Eloquent\\Builder))\n#9 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Builder.php(816): Illuminate\\Database\\Eloquent\\Model->save()\n#10 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Support\\helpers.php(263): Illuminate\\Database\\Eloquent\\Builder->Illuminate\\Database\\Eloquent\\{closure}(Object(App\\Models\\Blog\\Blog))\n#11 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Builder.php(817): tap(Object(App\\Models\\Blog\\Blog), Object(Closure))\n#12 E:\\site\\tf2.trainznation\\app\\Http\\Controllers\\Api\\Blog\\BlogController.php(118): Illuminate\\Database\\Eloquent\\Builder->create(Array)\n#13 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Controller.php(54): App\\Http\\Controllers\\Api\\Blog\\BlogController->store(Object(App\\Http\\Requests\\Blog\\BlogRequest))\n#14 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction('store', Array)\n#15 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(254): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(App\\Http\\Controllers\\Api\\Blog\\BlogController), 'store')\n#16 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(197): Illuminate\\Routing\\Route->runController()\n#17 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(693): Illuminate\\Routing\\Route->run()\n#18 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(128): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#19 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\SubstituteBindings.php(50): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#20 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(167): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#21 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\ThrottleRequests.php(127): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#22 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\ThrottleRequests.php(103): Illuminate\\Routing\\Middleware\\ThrottleRequests->handleRequest(Object(Illuminate\\Http\\Request), Object(Closure), Array)\n#23 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\ThrottleRequests.php(55): Illuminate\\Routing\\Middleware\\ThrottleRequests->handleRequestUsingNamedLimiter(Object(Illuminate\\Http\\Request), Object(Closure), 'api', Object(Closure))\n#24 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(167): Illuminate\\Routing\\Middleware\\ThrottleRequests->handle(Object(Illuminate\\Http\\Request), Object(Closure), 'api')\n#25 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(103): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#26 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(695): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#27 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(670): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#28 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(636): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#29 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(625): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#30 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(166): Illuminate\\Routing\\Router->dispatch(Object(Illuminate\\Http\\Request))\n#31 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(128): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#32 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(167): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#34 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#35 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(167): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#36 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#37 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(167): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\PreventRequestsDuringMaintenance.php(86): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(167): Illuminate\\Foundation\\Http\\Middleware\\PreventRequestsDuringMaintenance->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#40 E:\\site\\tf2.trainznation\\vendor\\fruitcake\\laravel-cors\\src\\HandleCors.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#41 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(167): Fruitcake\\Cors\\HandleCors->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#42 E:\\site\\tf2.trainznation\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#43 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(167): Fideloper\\Proxy\\TrustProxies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#44 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(103): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#45 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(141): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#46 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#47 E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(324): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#48 E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(305): Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelOrLumenRoute(Object(Illuminate\\Http\\Request))\n#49 E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(76): Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#50 E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(51): Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls->makeResponseCall(Object(Illuminate\\Routing\\Route), Array, Array)\n#51 E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(41): Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls->makeResponseCallIfEnabledAndNoSuccessResponses(Object(Illuminate\\Routing\\Route), Array, Array)\n#52 E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Generator.php(236): Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Illuminate\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)\n#53 E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Generator.php(172): Knuckles\\Scribe\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)\n#54 E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Generator.php(127): Knuckles\\Scribe\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Illuminate\\Routing\\Route), Array, Array)\n#55 E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Commands\\GenerateDocumentation.php(119): Knuckles\\Scribe\\Extracting\\Generator->processRoute(Object(Illuminate\\Routing\\Route), Array)\n#56 E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Commands\\GenerateDocumentation.php(73): Knuckles\\Scribe\\Commands\\GenerateDocumentation->processRoutes(Array)\n#57 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(36): Knuckles\\Scribe\\Commands\\GenerateDocumentation->handle(Object(Knuckles\\Scribe\\Matching\\RouteMatcher))\n#58 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(40): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#59 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(93): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#60 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(37): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#61 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(611): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#62 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(136): Illuminate\\Container\\Container->call(Array)\n#63 E:\\site\\tf2.trainznation\\vendor\\symfony\\console\\Command\\Command.php(256): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#64 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(121): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#65 E:\\site\\tf2.trainznation\\vendor\\symfony\\console\\Application.php(971): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#66 E:\\site\\tf2.trainznation\\vendor\\symfony\\console\\Application.php(290): Symfony\\Component\\Console\\Application->doRunCommand(Object(Knuckles\\Scribe\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#67 E:\\site\\tf2.trainznation\\vendor\\symfony\\console\\Application.php(166): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#68 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(92): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#69 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(129): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#70 E:\\site\\tf2.trainznation\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#71 {main}"
    }
}
```
<div id="execution-results-POSTapi-blog-post" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-blog-post"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-blog-post"></code></pre>
</div>
<div id="execution-error-POSTapi-blog-post" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-blog-post"></code></pre>
</div>
<form id="form-POSTapi-blog-post" data-method="POST" data-path="api/blog/post" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-blog-post', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-blog-post" onclick="tryItOut('POSTapi-blog-post');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-blog-post" onclick="cancelTryOut('POSTapi-blog-post');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-blog-post" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/blog/post</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>title</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="title" data-endpoint="POSTapi-blog-post" data-component="body" required  hidden>
<br>
Titre de l'article.</p>
<p>
<b><code>content</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="content" data-endpoint="POSTapi-blog-post" data-component="body" required  hidden>
<br>
Contenue de l'article.</p>
<p>
<b><code>blog_category_id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="blog_category_id" data-endpoint="POSTapi-blog-post" data-component="body" required  hidden>
<br>
ID de la catégorie parente.</p>

</form>


## Get Post


Affiche un article particulier

> Example request:

```bash
curl -X GET \
    -G "http://tf2.trainznation.io/api/blog/post/6" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://tf2.trainznation.io/api/blog/post/6"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://tf2.trainznation.io/api/blog/post/6',
    [
        'headers' => [
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "STATUS": "200",
    "MESSAGE": "Post",
    "DATA": {
        "id": 6,
        "title": "Quo eos et quaerat rerum porro. Rerum at eveniet omnis. Nihil nisi possimus est quas facilis.",
        "slug": "placeat-qui-velit-culpa-voluptas-amet-repellendus",
        "short": "Il se traînait à genoux vers elle, se déclarant envoyé par Edgar. Il avouait, du reste, de comprendre même la plus vague, l'abandonna. Mais, par un ma...",
        "content": "Il se traînait à genoux vers elle, se déclarant envoyé par Edgar. Il avouait, du reste, de comprendre même la plus vague, l'abandonna. Mais, par un matin d'automne, flottait au-dessus de sa trop.",
        "state": 2,
        "published": 1607613300,
        "created": "2021-03-24T15:15:00.000000Z",
        "updated": "2021-03-24T15:15:00.000000Z",
        "category": {
            "id": 1,
            "name": "Transport Fever 2",
            "image": "http:\/\/tf2.trainznation.io\/storage\/site\/news\/news_category_banner.png"
        },
        "image": "https:\/\/via.placeholder.com\/1920x1080"
    }
}
```
<div id="execution-results-GETapi-blog-post--post-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-blog-post--post-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-blog-post--post-"></code></pre>
</div>
<div id="execution-error-GETapi-blog-post--post-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-blog-post--post-"></code></pre>
</div>
<form id="form-GETapi-blog-post--post-" data-method="GET" data-path="api/blog/post/{post}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-blog-post--post-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-blog-post--post-" onclick="tryItOut('GETapi-blog-post--post-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-blog-post--post-" onclick="cancelTryOut('GETapi-blog-post--post-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-blog-post--post-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/blog/post/{post}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>post</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="post" data-endpoint="GETapi-blog-post--post-" data-component="url" required  hidden>
<br>
ID de l'article à afficher</p>
</form>


## Update Post


Met à jour un article particulier

> Example request:

```bash
curl -X PUT \
    "http://tf2.trainznation.io/api/blog/post/17" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"title":"Ceci est un titre","content":"Ceci est la description d'un article avec beaucoups de caract\u00e8res","blog_category_id":"est"}'

```

```javascript
const url = new URL(
    "http://tf2.trainznation.io/api/blog/post/17"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "title": "Ceci est un titre",
    "content": "Ceci est la description d'un article avec beaucoups de caract\u00e8res",
    "blog_category_id": "est"
}

fetch(url, {
    method: "PUT",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'http://tf2.trainznation.io/api/blog/post/17',
    [
        'headers' => [
            'Accept' => 'application/json',
        ],
        'json' => [
            'title' => 'Ceci est un titre',
            'content' => 'Ceci est la description d\'un article avec beaucoups de caractères',
            'blog_category_id' => 'est',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "STATUS": "500",
    "MESSAGE": "Erreur lors de la mise à jour du post",
    "DATA": {
        "code": "ERROR-500-2021-03-18-004",
        "error": "SQLSTATE[HY000]: General error: 1366 Incorrect integer value: 'est' for column 'blog_category_id' at row 1 (SQL: update `blogs` set `title` = Ceci est un titre, `slug` = ceci-est-un-titre, `content` = Ceci est la description d'un article avec beaucoups de caractères, `blog_category_id` = est, `blogs`.`updated_at` = 2021-03-24 17:07:16 where `id` = 17)",
        "trace": "#0 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(638): Illuminate\\Database\\Connection->runQueryCallback('update `blogs` ...', Array, Object(Closure))\n#1 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(503): Illuminate\\Database\\Connection->run('update `blogs` ...', Array, Object(Closure))\n#2 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Connection.php(436): Illuminate\\Database\\Connection->affectingStatement('update `blogs` ...', Array)\n#3 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Query\\Builder.php(2913): Illuminate\\Database\\Connection->update('update `blogs` ...', Array)\n#4 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Builder.php(841): Illuminate\\Database\\Query\\Builder->update(Array)\n#5 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Model.php(936): Illuminate\\Database\\Eloquent\\Builder->update(Array)\n#6 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Model.php(851): Illuminate\\Database\\Eloquent\\Model->performUpdate(Object(Illuminate\\Database\\Eloquent\\Builder))\n#7 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Database\\Eloquent\\Model.php(783): Illuminate\\Database\\Eloquent\\Model->save(Array)\n#8 E:\\site\\tf2.trainznation\\app\\Http\\Controllers\\Api\\Blog\\BlogController.php(177): Illuminate\\Database\\Eloquent\\Model->update(Array)\n#9 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Controller.php(54): App\\Http\\Controllers\\Api\\Blog\\BlogController->update(Object(App\\Http\\Requests\\Blog\\BlogRequest), Object(App\\Models\\Blog\\Blog))\n#10 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php(45): Illuminate\\Routing\\Controller->callAction('update', Array)\n#11 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(254): Illuminate\\Routing\\ControllerDispatcher->dispatch(Object(Illuminate\\Routing\\Route), Object(App\\Http\\Controllers\\Api\\Blog\\BlogController), 'update')\n#12 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php(197): Illuminate\\Routing\\Route->runController()\n#13 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(693): Illuminate\\Routing\\Route->run()\n#14 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(128): Illuminate\\Routing\\Router->Illuminate\\Routing\\{closure}(Object(Illuminate\\Http\\Request))\n#15 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\SubstituteBindings.php(50): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#16 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(167): Illuminate\\Routing\\Middleware\\SubstituteBindings->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#17 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\ThrottleRequests.php(127): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#18 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\ThrottleRequests.php(103): Illuminate\\Routing\\Middleware\\ThrottleRequests->handleRequest(Object(Illuminate\\Http\\Request), Object(Closure), Array)\n#19 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\ThrottleRequests.php(55): Illuminate\\Routing\\Middleware\\ThrottleRequests->handleRequestUsingNamedLimiter(Object(Illuminate\\Http\\Request), Object(Closure), 'api', Object(Closure))\n#20 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(167): Illuminate\\Routing\\Middleware\\ThrottleRequests->handle(Object(Illuminate\\Http\\Request), Object(Closure), 'api')\n#21 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(103): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#22 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(695): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#23 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(670): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Illuminate\\Http\\Request))\n#24 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(636): Illuminate\\Routing\\Router->runRoute(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#25 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(625): Illuminate\\Routing\\Router->dispatchToRoute(Object(Illuminate\\Http\\Request))\n#26 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(166): Illuminate\\Routing\\Router->dispatch(Object(Illuminate\\Http\\Request))\n#27 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(128): Illuminate\\Foundation\\Http\\Kernel->Illuminate\\Foundation\\Http\\{closure}(Object(Illuminate\\Http\\Request))\n#28 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#29 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(167): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#30 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#31 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(167): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#32 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#33 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(167): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#34 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\PreventRequestsDuringMaintenance.php(86): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#35 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(167): Illuminate\\Foundation\\Http\\Middleware\\PreventRequestsDuringMaintenance->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#36 E:\\site\\tf2.trainznation\\vendor\\fruitcake\\laravel-cors\\src\\HandleCors.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#37 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(167): Fruitcake\\Cors\\HandleCors->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#38 E:\\site\\tf2.trainznation\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#39 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(167): Fideloper\\Proxy\\TrustProxies->handle(Object(Illuminate\\Http\\Request), Object(Closure))\n#40 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(103): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))\n#41 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(141): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#42 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))\n#43 E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(324): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))\n#44 E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(305): Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelOrLumenRoute(Object(Illuminate\\Http\\Request))\n#45 E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(76): Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request), Object(Illuminate\\Routing\\Route))\n#46 E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(51): Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls->makeResponseCall(Object(Illuminate\\Routing\\Route), Array, Array)\n#47 E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(41): Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls->makeResponseCallIfEnabledAndNoSuccessResponses(Object(Illuminate\\Routing\\Route), Array, Array)\n#48 E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Generator.php(236): Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Illuminate\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)\n#49 E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Generator.php(172): Knuckles\\Scribe\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)\n#50 E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Generator.php(127): Knuckles\\Scribe\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Illuminate\\Routing\\Route), Array, Array)\n#51 E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Commands\\GenerateDocumentation.php(119): Knuckles\\Scribe\\Extracting\\Generator->processRoute(Object(Illuminate\\Routing\\Route), Array)\n#52 E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Commands\\GenerateDocumentation.php(73): Knuckles\\Scribe\\Commands\\GenerateDocumentation->processRoutes(Array)\n#53 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(36): Knuckles\\Scribe\\Commands\\GenerateDocumentation->handle(Object(Knuckles\\Scribe\\Matching\\RouteMatcher))\n#54 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(40): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#55 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(93): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#56 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(37): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#57 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(611): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#58 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(136): Illuminate\\Container\\Container->call(Array)\n#59 E:\\site\\tf2.trainznation\\vendor\\symfony\\console\\Command\\Command.php(256): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#60 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(121): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#61 E:\\site\\tf2.trainznation\\vendor\\symfony\\console\\Application.php(971): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#62 E:\\site\\tf2.trainznation\\vendor\\symfony\\console\\Application.php(290): Symfony\\Component\\Console\\Application->doRunCommand(Object(Knuckles\\Scribe\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#63 E:\\site\\tf2.trainznation\\vendor\\symfony\\console\\Application.php(166): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#64 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(92): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#65 E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(129): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#66 E:\\site\\tf2.trainznation\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#67 {main}"
    }
}
```
<div id="execution-results-PUTapi-blog-post--post-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTapi-blog-post--post-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-blog-post--post-"></code></pre>
</div>
<div id="execution-error-PUTapi-blog-post--post-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-blog-post--post-"></code></pre>
</div>
<form id="form-PUTapi-blog-post--post-" data-method="PUT" data-path="api/blog/post/{post}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-blog-post--post-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-blog-post--post-" onclick="tryItOut('PUTapi-blog-post--post-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-blog-post--post-" onclick="cancelTryOut('PUTapi-blog-post--post-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-blog-post--post-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/blog/post/{post}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>post</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="post" data-endpoint="PUTapi-blog-post--post-" data-component="url" required  hidden>
<br>
ID de l'article à modifier</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>title</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="title" data-endpoint="PUTapi-blog-post--post-" data-component="body" required  hidden>
<br>
Titre de l'article.</p>
<p>
<b><code>content</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="content" data-endpoint="PUTapi-blog-post--post-" data-component="body" required  hidden>
<br>
Contenue de l'article.</p>
<p>
<b><code>blog_category_id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="blog_category_id" data-endpoint="PUTapi-blog-post--post-" data-component="body" required  hidden>
<br>
ID de la catégorie parente.</p>

</form>


## Delete Post


Supprime un article

> Example request:

```bash
curl -X DELETE \
    "http://tf2.trainznation.io/api/blog/post/13" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://tf2.trainznation.io/api/blog/post/13"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "DELETE",
    headers,
}).then(response => response.json());
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'http://tf2.trainznation.io/api/blog/post/13',
    [
        'headers' => [
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "STATUS": "200",
    "MESSAGE": "Post Deleted",
    "DATA": []
}
```
<div id="execution-results-DELETEapi-blog-post--post-" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEapi-blog-post--post-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEapi-blog-post--post-"></code></pre>
</div>
<div id="execution-error-DELETEapi-blog-post--post-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEapi-blog-post--post-"></code></pre>
</div>
<form id="form-DELETEapi-blog-post--post-" data-method="DELETE" data-path="api/blog/post/{post}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEapi-blog-post--post-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEapi-blog-post--post-" onclick="tryItOut('DELETEapi-blog-post--post-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEapi-blog-post--post-" onclick="cancelTryOut('DELETEapi-blog-post--post-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEapi-blog-post--post-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>api/blog/post/{post}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>post</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="post" data-endpoint="DELETEapi-blog-post--post-" data-component="url" required  hidden>
<br>
ID de l'article à supprimer</p>
</form>


## List Category


Affiche la liste des categories du blog

> Example request:

```bash
curl -X GET \
    -G "http://tf2.trainznation.io/api/blog/category" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://tf2.trainznation.io/api/blog/category"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://tf2.trainznation.io/api/blog/category',
    [
        'headers' => [
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "STATUS": "200",
    "MESSAGE": "Blog Category List",
    "DATA": [
        {
            "id": 1,
            "name": "Transport Fever 2",
            "image": "http:\/\/tf2.trainznation.io\/storage\/site\/news\/news_category_banner.png"
        },
        {
            "id": 2,
            "name": "Trainznation Studio",
            "image": "http:\/\/tf2.trainznation.io\/storage\/site\/news\/news_category_banner.png"
        },
        {
            "id": 3,
            "name": "Développement",
            "image": "http:\/\/tf2.trainznation.io\/storage\/site\/news\/news_category_banner.png"
        }
    ]
}
```
<div id="execution-results-GETapi-blog-category" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-blog-category"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-blog-category"></code></pre>
</div>
<div id="execution-error-GETapi-blog-category" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-blog-category"></code></pre>
</div>
<form id="form-GETapi-blog-category" data-method="GET" data-path="api/blog/category" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-blog-category', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-blog-category" onclick="tryItOut('GETapi-blog-category');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-blog-category" onclick="cancelTryOut('GETapi-blog-category');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-blog-category" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/blog/category</code></b>
</p>
</form>


## Create Category




> Example request:

```bash
curl -X POST \
    "http://tf2.trainznation.io/api/blog/category" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"Mat\u00e9riel Roulant"}'

```

```javascript
const url = new URL(
    "http://tf2.trainznation.io/api/blog/category"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "Mat\u00e9riel Roulant"
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://tf2.trainznation.io/api/blog/category',
    [
        'headers' => [
            'Accept' => 'application/json',
        ],
        'json' => [
            'name' => 'Matériel Roulant',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "STATUS": "200",
    "MESSAGE": "Create Blog Category",
    "DATA": {
        "id": 6,
        "name": "Matériel Roulant",
        "posts": [],
        "image": "http:\/\/tf2.trainznation.io\/storage\/site\/news\/news_category_banner.png"
    }
}
```
<div id="execution-results-POSTapi-blog-category" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-blog-category"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-blog-category"></code></pre>
</div>
<div id="execution-error-POSTapi-blog-category" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-blog-category"></code></pre>
</div>
<form id="form-POSTapi-blog-category" data-method="POST" data-path="api/blog/category" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-blog-category', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-blog-category" onclick="tryItOut('POSTapi-blog-category');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-blog-category" onclick="cancelTryOut('POSTapi-blog-category');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-blog-category" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/blog/category</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="name" data-endpoint="POSTapi-blog-category" data-component="body" required  hidden>
<br>
Nom de la catégorie.</p>

</form>

### Response
<h4 class="fancy-heading-panel"><b>Response Fields</b></h4>
<p>
<b><code>name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<br>
Nom de la catégorie</p>
<p>
<b><code>id</code></b>&nbsp;&nbsp;  &nbsp;
<br>
Id de la categorie</p>

## Get Category


Affiche les informations d'une categorie particulière

> Example request:

```bash
curl -X GET \
    -G "http://tf2.trainznation.io/api/blog/category/13" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://tf2.trainznation.io/api/blog/category/13"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://tf2.trainznation.io/api/blog/category/13',
    [
        'headers' => [
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (404):

```json
{
    "message": "No query results for model [App\\Models\\Blog\\BlogCategory] 13",
    "exception": "Symfony\\Component\\HttpKernel\\Exception\\NotFoundHttpException",
    "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Exceptions\\Handler.php",
    "line": 381,
    "trace": [
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Exceptions\\Handler.php",
            "line": 330,
            "function": "prepareException",
            "class": "Illuminate\\Foundation\\Exceptions\\Handler",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\nunomaduro\\collision\\src\\Adapters\\Laravel\\ExceptionHandler.php",
            "line": 54,
            "function": "render",
            "class": "Illuminate\\Foundation\\Exceptions\\Handler",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php",
            "line": 51,
            "function": "render",
            "class": "NunoMaduro\\Collision\\Adapters\\Laravel\\ExceptionHandler",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 172,
            "function": "handleException",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\ThrottleRequests.php",
            "line": 127,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\ThrottleRequests.php",
            "line": 103,
            "function": "handleRequest",
            "class": "Illuminate\\Routing\\Middleware\\ThrottleRequests",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\ThrottleRequests.php",
            "line": 55,
            "function": "handleRequestUsingNamedLimiter",
            "class": "Illuminate\\Routing\\Middleware\\ThrottleRequests",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Routing\\Middleware\\ThrottleRequests",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 103,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php",
            "line": 695,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php",
            "line": 670,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php",
            "line": 636,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php",
            "line": 625,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php",
            "line": 166,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 128,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php",
            "line": 21,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php",
            "line": 21,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\PreventRequestsDuringMaintenance.php",
            "line": 86,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\PreventRequestsDuringMaintenance",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\fruitcake\\laravel-cors\\src\\HandleCors.php",
            "line": 57,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fruitcake\\Cors\\HandleCors",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\fideloper\\proxy\\src\\TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 103,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php",
            "line": 141,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php",
            "line": 110,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php",
            "line": 324,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php",
            "line": 305,
            "function": "callLaravelOrLumenRoute",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php",
            "line": 76,
            "function": "makeApiCall",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php",
            "line": 51,
            "function": "makeResponseCall",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php",
            "line": 41,
            "function": "makeResponseCallIfEnabledAndNoSuccessResponses",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Generator.php",
            "line": 236,
            "function": "__invoke",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Generator.php",
            "line": 172,
            "function": "iterateThroughStrategies",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Generator.php",
            "line": 127,
            "function": "fetchResponses",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Commands\\GenerateDocumentation.php",
            "line": 119,
            "function": "processRoute",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Commands\\GenerateDocumentation.php",
            "line": 73,
            "function": "processRoutes",
            "class": "Knuckles\\Scribe\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php",
            "line": 36,
            "function": "handle",
            "class": "Knuckles\\Scribe\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php",
            "line": 40,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php",
            "line": 93,
            "function": "unwrapIfClosure",
            "class": "Illuminate\\Container\\Util",
            "type": "::"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php",
            "line": 37,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php",
            "line": 611,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php",
            "line": 136,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\symfony\\console\\Command\\Command.php",
            "line": 256,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php",
            "line": 121,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\symfony\\console\\Application.php",
            "line": 971,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\symfony\\console\\Application.php",
            "line": 290,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\symfony\\console\\Application.php",
            "line": 166,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php",
            "line": 92,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php",
            "line": 129,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```
<div id="execution-results-GETapi-blog-category--category-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-blog-category--category-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-blog-category--category-"></code></pre>
</div>
<div id="execution-error-GETapi-blog-category--category-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-blog-category--category-"></code></pre>
</div>
<form id="form-GETapi-blog-category--category-" data-method="GET" data-path="api/blog/category/{category}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-blog-category--category-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-blog-category--category-" onclick="tryItOut('GETapi-blog-category--category-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-blog-category--category-" onclick="cancelTryOut('GETapi-blog-category--category-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-blog-category--category-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/blog/category/{category}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>category</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="category" data-endpoint="GETapi-blog-category--category-" data-component="url" required  hidden>
<br>
Id de la category.</p>
</form>


## Update Category


Mise à jour d'une catégorie

> Example request:

```bash
curl -X PUT \
    "http://tf2.trainznation.io/api/blog/category/7" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"name":"Mat\u00e9riel Roulant"}'

```

```javascript
const url = new URL(
    "http://tf2.trainznation.io/api/blog/category/7"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "name": "Mat\u00e9riel Roulant"
}

fetch(url, {
    method: "PUT",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->put(
    'http://tf2.trainznation.io/api/blog/category/7',
    [
        'headers' => [
            'Accept' => 'application/json',
        ],
        'json' => [
            'name' => 'Matériel Roulant',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (404):

```json
{
    "message": "No query results for model [App\\Models\\Blog\\BlogCategory] 7",
    "exception": "Symfony\\Component\\HttpKernel\\Exception\\NotFoundHttpException",
    "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Exceptions\\Handler.php",
    "line": 381,
    "trace": [
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Exceptions\\Handler.php",
            "line": 330,
            "function": "prepareException",
            "class": "Illuminate\\Foundation\\Exceptions\\Handler",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\nunomaduro\\collision\\src\\Adapters\\Laravel\\ExceptionHandler.php",
            "line": 54,
            "function": "render",
            "class": "Illuminate\\Foundation\\Exceptions\\Handler",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php",
            "line": 51,
            "function": "render",
            "class": "NunoMaduro\\Collision\\Adapters\\Laravel\\ExceptionHandler",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 172,
            "function": "handleException",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\ThrottleRequests.php",
            "line": 127,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\ThrottleRequests.php",
            "line": 103,
            "function": "handleRequest",
            "class": "Illuminate\\Routing\\Middleware\\ThrottleRequests",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\ThrottleRequests.php",
            "line": 55,
            "function": "handleRequestUsingNamedLimiter",
            "class": "Illuminate\\Routing\\Middleware\\ThrottleRequests",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Routing\\Middleware\\ThrottleRequests",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 103,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php",
            "line": 695,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php",
            "line": 670,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php",
            "line": 636,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php",
            "line": 625,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php",
            "line": 166,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 128,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php",
            "line": 21,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php",
            "line": 21,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\PreventRequestsDuringMaintenance.php",
            "line": 86,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\PreventRequestsDuringMaintenance",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\fruitcake\\laravel-cors\\src\\HandleCors.php",
            "line": 57,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fruitcake\\Cors\\HandleCors",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\fideloper\\proxy\\src\\TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 103,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php",
            "line": 141,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php",
            "line": 110,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php",
            "line": 324,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php",
            "line": 305,
            "function": "callLaravelOrLumenRoute",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php",
            "line": 76,
            "function": "makeApiCall",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php",
            "line": 51,
            "function": "makeResponseCall",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php",
            "line": 41,
            "function": "makeResponseCallIfEnabledAndNoSuccessResponses",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Generator.php",
            "line": 236,
            "function": "__invoke",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Generator.php",
            "line": 172,
            "function": "iterateThroughStrategies",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Generator.php",
            "line": 127,
            "function": "fetchResponses",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Commands\\GenerateDocumentation.php",
            "line": 119,
            "function": "processRoute",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Commands\\GenerateDocumentation.php",
            "line": 73,
            "function": "processRoutes",
            "class": "Knuckles\\Scribe\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php",
            "line": 36,
            "function": "handle",
            "class": "Knuckles\\Scribe\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php",
            "line": 40,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php",
            "line": 93,
            "function": "unwrapIfClosure",
            "class": "Illuminate\\Container\\Util",
            "type": "::"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php",
            "line": 37,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php",
            "line": 611,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php",
            "line": 136,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\symfony\\console\\Command\\Command.php",
            "line": 256,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php",
            "line": 121,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\symfony\\console\\Application.php",
            "line": 971,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\symfony\\console\\Application.php",
            "line": 290,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\symfony\\console\\Application.php",
            "line": 166,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php",
            "line": 92,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php",
            "line": 129,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```
<div id="execution-results-PUTapi-blog-category--category-" hidden>
    <blockquote>Received response<span id="execution-response-status-PUTapi-blog-category--category-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-PUTapi-blog-category--category-"></code></pre>
</div>
<div id="execution-error-PUTapi-blog-category--category-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-PUTapi-blog-category--category-"></code></pre>
</div>
<form id="form-PUTapi-blog-category--category-" data-method="PUT" data-path="api/blog/category/{category}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('PUTapi-blog-category--category-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-PUTapi-blog-category--category-" onclick="tryItOut('PUTapi-blog-category--category-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-PUTapi-blog-category--category-" onclick="cancelTryOut('PUTapi-blog-category--category-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-PUTapi-blog-category--category-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-darkblue">PUT</small>
 <b><code>api/blog/category/{category}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>category</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="category" data-endpoint="PUTapi-blog-category--category-" data-component="url" required  hidden>
<br>
Id de la category à modifier.</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>name</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="name" data-endpoint="PUTapi-blog-category--category-" data-component="body" required  hidden>
<br>
Nom de la catégorie.</p>

</form>


## Delete Category


Supprime une categorie

> Example request:

```bash
curl -X DELETE \
    "http://tf2.trainznation.io/api/blog/category/18" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://tf2.trainznation.io/api/blog/category/18"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "DELETE",
    headers,
}).then(response => response.json());
```

```php

$client = new \GuzzleHttp\Client();
$response = $client->delete(
    'http://tf2.trainznation.io/api/blog/category/18',
    [
        'headers' => [
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (404):

```json
{
    "message": "No query results for model [App\\Models\\Blog\\BlogCategory] 18",
    "exception": "Symfony\\Component\\HttpKernel\\Exception\\NotFoundHttpException",
    "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Exceptions\\Handler.php",
    "line": 381,
    "trace": [
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Exceptions\\Handler.php",
            "line": 330,
            "function": "prepareException",
            "class": "Illuminate\\Foundation\\Exceptions\\Handler",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\nunomaduro\\collision\\src\\Adapters\\Laravel\\ExceptionHandler.php",
            "line": 54,
            "function": "render",
            "class": "Illuminate\\Foundation\\Exceptions\\Handler",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php",
            "line": 51,
            "function": "render",
            "class": "NunoMaduro\\Collision\\Adapters\\Laravel\\ExceptionHandler",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 172,
            "function": "handleException",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\ThrottleRequests.php",
            "line": 127,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\ThrottleRequests.php",
            "line": 103,
            "function": "handleRequest",
            "class": "Illuminate\\Routing\\Middleware\\ThrottleRequests",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\ThrottleRequests.php",
            "line": 55,
            "function": "handleRequestUsingNamedLimiter",
            "class": "Illuminate\\Routing\\Middleware\\ThrottleRequests",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Routing\\Middleware\\ThrottleRequests",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 103,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php",
            "line": 695,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php",
            "line": 670,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php",
            "line": 636,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php",
            "line": 625,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php",
            "line": 166,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 128,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php",
            "line": 21,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php",
            "line": 21,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\PreventRequestsDuringMaintenance.php",
            "line": 86,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\PreventRequestsDuringMaintenance",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\fruitcake\\laravel-cors\\src\\HandleCors.php",
            "line": 57,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fruitcake\\Cors\\HandleCors",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\fideloper\\proxy\\src\\TrustProxies.php",
            "line": 57,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 167,
            "function": "handle",
            "class": "Fideloper\\Proxy\\TrustProxies",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 103,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php",
            "line": 141,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php",
            "line": 110,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php",
            "line": 324,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php",
            "line": 305,
            "function": "callLaravelOrLumenRoute",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php",
            "line": 76,
            "function": "makeApiCall",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php",
            "line": 51,
            "function": "makeResponseCall",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php",
            "line": 41,
            "function": "makeResponseCallIfEnabledAndNoSuccessResponses",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Generator.php",
            "line": 236,
            "function": "__invoke",
            "class": "Knuckles\\Scribe\\Extracting\\Strategies\\Responses\\ResponseCalls",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Generator.php",
            "line": 172,
            "function": "iterateThroughStrategies",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Extracting\\Generator.php",
            "line": 127,
            "function": "fetchResponses",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Commands\\GenerateDocumentation.php",
            "line": 119,
            "function": "processRoute",
            "class": "Knuckles\\Scribe\\Extracting\\Generator",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\knuckleswtf\\scribe\\src\\Commands\\GenerateDocumentation.php",
            "line": 73,
            "function": "processRoutes",
            "class": "Knuckles\\Scribe\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php",
            "line": 36,
            "function": "handle",
            "class": "Knuckles\\Scribe\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php",
            "line": 40,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php",
            "line": 93,
            "function": "unwrapIfClosure",
            "class": "Illuminate\\Container\\Util",
            "type": "::"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php",
            "line": 37,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php",
            "line": 611,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php",
            "line": 136,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\symfony\\console\\Command\\Command.php",
            "line": 256,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php",
            "line": 121,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\symfony\\console\\Application.php",
            "line": 971,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\symfony\\console\\Application.php",
            "line": 290,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\symfony\\console\\Application.php",
            "line": 166,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php",
            "line": 92,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php",
            "line": 129,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "E:\\site\\tf2.trainznation\\artisan",
            "line": 37,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```
<div id="execution-results-DELETEapi-blog-category--category-" hidden>
    <blockquote>Received response<span id="execution-response-status-DELETEapi-blog-category--category-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-DELETEapi-blog-category--category-"></code></pre>
</div>
<div id="execution-error-DELETEapi-blog-category--category-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-DELETEapi-blog-category--category-"></code></pre>
</div>
<form id="form-DELETEapi-blog-category--category-" data-method="DELETE" data-path="api/blog/category/{category}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('DELETEapi-blog-category--category-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-DELETEapi-blog-category--category-" onclick="tryItOut('DELETEapi-blog-category--category-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-DELETEapi-blog-category--category-" onclick="cancelTryOut('DELETEapi-blog-category--category-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-DELETEapi-blog-category--category-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-red">DELETE</small>
 <b><code>api/blog/category/{category}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>category</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="category" data-endpoint="DELETEapi-blog-category--category-" data-component="url" required  hidden>
<br>
ID de la catégorie.</p>
</form>



