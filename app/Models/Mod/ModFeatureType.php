<?php

namespace App\Models\Mod;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModFeatureType extends Model
{
    use HasFactory;
    protected $guarded = [];
    public $timestamps = false;

    public function features()
    {
        return $this->hasMany(ModFeature::class);
    }
}
