<?php

namespace App\Models\Mod;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModChangelog extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function mod() {
        return $this->belongsTo(Mod::class);
    }
}
