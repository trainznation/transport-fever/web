<?php

namespace App\Models\Mod;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModTag extends Model
{
    use HasFactory;

    protected $guarded = [];
    public $timestamps = false;

    public function mod()
    {
        return $this->belongsTo(Mod::class);
    }
}
