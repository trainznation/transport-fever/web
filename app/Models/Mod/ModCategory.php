<?php

namespace App\Models\Mod;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModCategory extends Model
{
    use HasFactory;
    protected $guarded = [];
    public $timestamps = false;

    public function mods() {
        return $this->hasMany(Mod::class);
    }
}
