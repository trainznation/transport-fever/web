<?php

namespace App\Models\Mod;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModNews extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $dates = ["created_at", "updated_at", "published_at"];

    public function mod()
    {
        return $this->belongsTo(Mod::class);
    }
}
