<?php

namespace App\Models\Mod;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mod extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $dates = ["created_at", "updated_at", "published_at"];

    public function category() {
        return $this->belongsTo(ModCategory::class, 'mod_category_id');
    }

    public function features() {
        return $this->hasMany(ModFeature::class);
    }

    public function changelogs()
    {
        return $this->hasMany(ModChangelog::class);
    }

    public function files()
    {
        return $this->hasMany(ModFile::class);
    }

    public function news()
    {
        return $this->hasMany(ModNews::class);
    }

    public function tags() {
        return $this->hasMany(ModTag::class);
    }
}
