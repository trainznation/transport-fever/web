<?php

namespace App\Models\Mod;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModFeature extends Model
{
    use HasFactory;
    protected $guarded = [];
    public $timestamps = false;

    public function mod() {
        return $this->belongsTo(Mod::class);
    }

    public function type()
    {
        return $this->belongsTo(ModFeatureType::class);
    }
}
