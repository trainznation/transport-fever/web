<?php

namespace App\Models\Blog;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
    use HasFactory;
    protected $guarded = [];
    public $timestamps = false;

    public function posts()
    {
        return $this->hasMany(Blog::class);
    }
}
