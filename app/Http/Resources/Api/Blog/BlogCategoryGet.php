<?php


namespace App\Http\Resources\Api\Blog;


use Illuminate\Support\Facades\Storage;

class BlogCategoryGet implements \App\Http\Resources\TrainzResources
{

    /**
     * @var \Illuminate\Contracts\Filesystem\Filesystem|\Illuminate\Filesystem\FilesystemAdapter
     */
    private $storage;

    public function __construct()
    {
        $this->storage = Storage::disk('public');
    }

    public function toArray($data)
    {
        return [
            "id" => $data->id,
            "name" => $data->name,
            "posts" => $data->posts,
            "image" => $this->storage->url('site/news/news_category_banner.png')
        ];
    }
}
