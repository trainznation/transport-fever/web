<?php


namespace App\Http\Resources\Api\Blog;


use Illuminate\Support\Facades\Storage;

class BlogCategoryList implements \App\Http\Resources\TrainzResources
{

    private $storage;

    public function __construct()
    {
        $this->storage = Storage::disk('public');
    }

    public function toArray($data)
    {
        $array = [];
        foreach ($data as $datum) {
            $array[] = [
                "id" => $datum->id,
                "name" => $datum->name,
                "image" => $this->storage->url('site/news/news_category_banner.png')
            ];
        }

        return $array;
    }
}
