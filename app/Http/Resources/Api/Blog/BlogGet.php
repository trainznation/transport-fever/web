<?php


namespace App\Http\Resources\Api\Blog;


use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class BlogGet implements \App\Http\Resources\TrainzResources
{

    /**
     * @var \Illuminate\Contracts\Filesystem\Filesystem|\Illuminate\Filesystem\FilesystemAdapter
     */
    private $storage;

    public function __construct()
    {
        $this->storage = Storage::disk('public');
    }

    public function toArray($data)
    {
        return [
            "id" => $data->id,
            "title" => $data->title,
            "slug" => $data->slug,
            "short" => Str::limit($data->content, 150),
            "content" => $data->content,
            "state" => $data->state,
            "published" => strtotime($data->published_at),
            "created" => $data->created_at,
            "updated" => $data->updated_at,
            "category" => [
                "id" => $data->category->id,
                "name" => $data->category->name,
                "image" => $this->storage->url('site/news/news_category_banner.png')
            ],
            "image" => ($this->storage->exists('blog/'.$data->id.'.png') == true) ? $this->storage->url("blog/".$data->id.".png") : 'https://via.placeholder.com/1920x1080'
        ];
    }
}
