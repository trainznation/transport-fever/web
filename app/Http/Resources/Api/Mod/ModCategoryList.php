<?php


namespace App\Http\Resources\Api\Mod;


use Illuminate\Support\Str;

class ModCategoryList implements \App\Http\Resources\TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($datas)
    {
        $array = [];
        foreach ($datas as $data) {
            $array[] = [
                "id" => $data->id,
                "name" => $data->name,
                "slug" => Str::slug($data->name),
                "mods" => $data->mods
            ];
        }

        return $array;
    }
}
