<?php


namespace App\Http\Resources\Api\Mod;


use App\Helpers\Converter;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ModList implements \App\Http\Resources\TrainzResources
{

    /**
     * @var \Illuminate\Contracts\Filesystem\Filesystem|\Illuminate\Filesystem\FilesystemAdapter
     */
    private $storage;

    public function __construct()
    {
        $this->storage = Storage::disk('public');
    }

    public function toArray($datas)
    {
        $array = [];
        foreach ($datas as $data) {
            $array[] = [
                "id" => $data->id,
                "name" => $data->name,
                "mod_id" => $data->mod_id,
                "mod_version" => $data->mod_version,
                "state" => $data->state,
                "mod_size" => [
                    "brut" => $data->mod_size,
                    "format" => Converter::convOctet($data->mod_size)
                ],
                "short" => Str::limit($data->description, 150),
                "description" => $data->description,
                "published" => $data->published,
                "published_at" => ($data->published !== 0) ? [
                    "brut" => $data->published_at,
                    "timestamp" => strtotime($data->published_at),
                    "format" => $data->published_at->format('d/m/Y à H:i'),
                    "humans" => $data->published_at->diffForHumans()
                ] : [
                    "brut" => null,
                    "timestamp" => null,
                    "format" => null,
                    "humans" => null
                ],
                "images" => [
                    "banner_extra_small" => $this->storage->exists('mod/images/'.$data->id.'/banner_extra_small.png') ? $this->storage->url('mod/images/'.$data->id.'/banner_extra_small.png') : 'https://picsum.photos/169/96',
                    "banner_small" => $this->storage->exists('mod/images/'.$data->id.'/banner_small.png') ? $this->storage->url('mod/images/'.$data->id.'/banner_small.png') : 'https://picsum.photos/960/254',
                    "banner" => $this->storage->exists('mod/images/'.$data->id.'/banner.png') ? $this->storage->url('mod/images/'.$data->id.'/banner.png') : 'https://picsum.photos/1920/509',
                    "banner_large" => $this->storage->exists('mod/images/'.$data->id.'/banner_large.png') ? $this->storage->url('mod/images/'.$data->id.'/banner_large.png') : 'https://picsum.photos/3840/1018',
                    "wall_small" => $this->storage->exists('mod/images/'.$data->id.'/wall_small.png') ? $this->storage->url('mod/images/'.$data->id.'/wall_small.png') : 'https://picsum.photos/960/540',
                    "wall" => $this->storage->exists('mod/images/'.$data->id.'/wall.png') ? $this->storage->url('mod/images/'.$data->id.'/wall.png') : 'https://picsum.photos/1920/1080',
                    "wall_large" => $this->storage->exists('mod/images/'.$data->id.'/wall_large.png') ? $this->storage->url('mod/images/'.$data->id.'/wall_large.png') : 'https://picsum.photos/3840/2160',
                    "icon_x_small" => $this->storage->exists('mod/images/'.$data->id.'/icon_x_small.png') ? $this->storage->url('mod/images/'.$data->id.'/icon_x_small.png') : 'https://picsum.photos/512/512',
                    "icon_small" => $this->storage->exists('mod/images/'.$data->id.'/icon_small.png') ? $this->storage->url('mod/images/'.$data->id.'/icon_x_small.png') : 'https://picsum.photos/682/682',
                    "icon" => $this->storage->exists('mod/images/'.$data->id.'/icon.png') ? $this->storage->url('mod/images/'.$data->id.'/icon.png') : 'https://picsum.photos/1024/1024',
                    "icon_large" => $this->storage->exists('mod/images/'.$data->id.'/icon_large.png') ? $this->storage->url('mod/images/'.$data->id.'/icon_large.png') : 'https://picsum.photos/2048/2048',
                ],
                "download_file" => $this->storage->exists('mod/packages/'.$data->id."/".$data->mod_id.".zip") ? $this->storage->url('mod/packages/'.$data->id.'/'.$data->mod_id.'.zip') : 'http://ovh.net/files/10Mio.dat',
                "category" => $data->category,
                "features" => $data->features,
                "news" => $data->news,
                "changelogs" => $data->changelogs,
                "files" => $data->files,
                "tags" => $data->tags
            ];
        }
        return $array;
    }
}
