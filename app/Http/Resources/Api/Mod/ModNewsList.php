<?php


namespace App\Http\Resources\Api\Mod;


use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ModNewsList implements \App\Http\Resources\TrainzResources
{

    /**
     * @var \Illuminate\Contracts\Filesystem\Filesystem|\Illuminate\Filesystem\FilesystemAdapter
     */
    private $storage;

    public function __construct()
    {
        $this->storage = Storage::disk('public');
    }

    public function toArray($datas)
    {
        $array = [];
        foreach ($datas as $data) {
            $array[] = [
                "id" => $data->id,
                "title" => $data->title,
                "slug" => $data->slug,
                "short" => Str::limit($data->content, 100),
                "content" => $data->content,
                "state" => $data->state,
                "published_at" => ($data->state !== 0) ? [
                    "brut" => $data->published_at,
                    "timestamp" => strtotime($data->published_at),
                    "format" => $data->published_at->format('d/m/Y à H:i'),
                    "humans" => $data->published_at->diffForHumans()
                ] : [
                    "brut" => null,
                    "timestamp" => null,
                    "format" => null,
                    "humans" => null
                ],
                "images" => $this->storage->exists('mod/images/'.$data->mod->id.'/news/'.$data->id.'.png') ? $this->storage->url('mod/images/'.$data->mod->id.'/news/'.$data->id.'.png') : 'https://picsum.photos/1920/1080',
                "mod_id" => $data->mod_id
            ];
        }

        return $array;
    }
}
