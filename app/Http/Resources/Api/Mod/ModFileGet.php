<?php


namespace App\Http\Resources\Api\Mod;


use Illuminate\Support\Facades\Storage;

class ModFileGet implements \App\Http\Resources\TrainzResources
{

    /**
     * @var \Illuminate\Contracts\Filesystem\Filesystem|\Illuminate\Filesystem\FilesystemAdapter
     */
    private $storage;

    public function __construct()
    {
        $this->storage = Storage::disk('public');
    }

    public function toArray($data)
    {
        return [
            "nameFile" => $data->nameFile,
            "type" => $data->type,
            "file_uri" => $this->getUriFromFileType($data->nameFile, $data->type)
        ];
    }

    private function getUriFromFileType($nameFile, $type)
    {
        switch ($type) {
            case 'image':
                if($this->storage->exists('gallery/images/'.$nameFile) == true) {
                    return $this->storage->url('gallery/images/'.$nameFile);
                } else {
                    return 'https://picsum.photos/1920/1080';
                }
                break;

            case 'video':
                if($this->storage->exists('gallery/video/'.$nameFile) == true) {
                    return $this->storage->url('gallery/video/'.$nameFile);
                } else {
                    return 'https://irrinews.com/wp-content/uploads/2016/11/Lorem-Ipsum-video.mp4';
                }
                break;
        }
    }
}
