<?php


namespace App\Http\Resources\Api\Mod;


use Illuminate\Support\Str;

class ModCategoryGet implements \App\Http\Resources\TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($data)
    {
        return [
            "id" => $data->id,
            "name" => $data->name,
            "slug" => Str::slug($data->name),
            "mods" => $data->mods
        ];
    }
}
