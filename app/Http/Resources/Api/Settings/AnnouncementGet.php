<?php


namespace App\Http\Resources\Api\Settings;


class AnnouncementGet implements \App\Http\Resources\TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($data)
    {
        return [
            "id" => $data->id,
            "title" => $data->title,
            "type" => $data->type,
            "content" => $data->content,
            "expiring" => $data->expiring_at,
            "created" => $data->created_at,
            "updated" => $data->updated_at
        ];
    }
}
