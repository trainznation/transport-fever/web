<?php


namespace App\Http\Resources\Api\Settings;


class AnnouncementList implements \App\Http\Resources\TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($datas)
    {
        $array = [];
        foreach ($datas as $data) {
            $array[] = [
                "id" => $data->id,
                "title" => $data->title,
                "type" => $data->type,
                "content" => $data->content,
                "expiring" => $data->expiring_at,
                "created" => $data->created_at,
                "updated" => $data->updated_at
            ];
        }

        return $array;
    }
}
