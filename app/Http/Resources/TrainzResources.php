<?php


namespace App\Http\Resources;


interface TrainzResources
{
    public function __construct();

    public function toArray($data);
}
