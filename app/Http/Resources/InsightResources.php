<?php


namespace App\Http\Resources;


use App\Helpers\Stringify;
use Illuminate\Support\Str;

class InsightResources
{
    public function toArray($datas)
    {
        return (object) [
            "summary" => [
                "code" => $datas->summary->code,
                "complexity" => $datas->summary->complexity,
                "architecture" => $datas->summary->architecture,
                "style" => $datas->summary->style,
            ],
            "code" => (object) $this->code($datas->Code),
            "complexity" => (object) $this->code($datas->Complexity),
            "architecture" => (object) $this->code($datas->Architecture),
            "style" => (object) $this->code($datas->Style),
        ];
    }

    private function code($datas)
    {
        $array = [];
        foreach ($datas as $data) {
            $array[] = [
                "stringify" => Stringify::randomString(),
                "title" => $data->title,
                "insightClass" => $data->insightClass,
                "file" => isset($data->file) ? $data->file : null,
                "line" => isset($data->line) ? $data->line : null,
                "message" => isset($data->message) ? $data->message : null
            ];
        }

        return $array;
    }
}
