<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;

class SettingAnnounceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => "required|string",
            "type" => "required|string",
            "content" => "string",
            "expiring_at" => "date"
        ];
    }

    public function bodyParameters()
    {
        return [
            'title' => [
                'description' => "Titre de l'annonce",
                'example' => "Ceci est un titre",
                "required"
            ],
            'type' => [
                "description" => "Type de l'annonce (Choix Disponible: success, info, danger, warning)",
                "example" => 'info',
                'required'
            ],
            'content' => [
                "description" => "Contenue de l'annonce, si disponible"
            ],
            'expiring_at' => [
                "description" => "Date de l'expiration de l'annonce. Cette annonce sera supprimer au terme de ce champs.",
                "example" => "2021-08-12 12:00:00"
            ]
        ];
    }
}
