<?php

namespace App\Http\Requests\Mod;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ModCategoryRequest
 * @package App\Http\Requests\Mod
 */

class ModCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|string"
        ];
    }

    public function bodyParameters()
    {
        return [
            'name' => [
                'description' => 'Nom de la catégorie',
                'example' => "Wagon"
            ],
        ];
    }
}
