<?php

namespace App\Http\Requests\Mod;

use Illuminate\Foundation\Http\FormRequest;

class ModFileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "nameFile" => "required|string",
            "type" => "required|string"
        ];
    }

    public function bodyParameters()
    {
        return [
            'nameFile' => [
                'description' => 'Nom du fichier avec extension',
                'example' => "image.jpg"
            ],
            'type' => [
                'description' => 'Type de fichier (image|video|zip|file)',
                'example' => "image"
            ],
        ];
    }
}
