<?php

namespace App\Http\Requests\Mod;

use Illuminate\Foundation\Http\FormRequest;

class ModRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|string",
            "mod_id" => "required|string",
            "mod_version" => "required|string",
            "description" => "required|string",
            "mod_category_id" => "required|integer"
        ];
    }

    public function bodyParameters()
    {
        return [
            'name' => [
                'description' => 'Nom du Mod',
                'example' => "Sncf X72500"
            ],
            "mod_id" => [
                "description" => "identifiant unique du mod",
                "example" => "trainznation_x72500_1"
            ],
            'mod_version' => [
                'description' => 'Version du mod au moment de sa publication',
                'example' => "1.0"
            ],
            'description' => [
                'description' => 'Description du Mod',
                'example' => "Nullam consectetur gravida ex, ac volutpat enim tincidunt vitae. Curabitur mi ex, mollis nec lorem nec, mattis molestie lectus. Quisque vel nunc tortor. Vivamus in ex felis. In elementum imperdiet tellus at imperdiet. Donec viverra eros eget ligula vulputate, sed hendrerit sapien vulputate. Aliquam vel pulvinar metus. Nulla nibh tortor, placerat ut dolor euismod, aliquam auctor lacus."
            ],
            'mod_category_id' => [
                'description' => 'Id de la catégorie du mod',
                'example' => "1"
            ],
        ];
    }
}
