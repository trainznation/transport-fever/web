<?php

namespace App\Http\Requests\Mod;

use Illuminate\Foundation\Http\FormRequest;

class ModNewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => "required|string",
            "content" => "required|string",
        ];
    }

    public function bodyParameters()
    {
        return [
            'title' => [
                'description' => 'Titre de la news',
                'example' => "Nouvelle mise à jour du mod"
            ],
            'content' => [
                'description' => 'Contenue de la news',
                'example' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sodales euismod congue. Integer hendrerit nisi dolor. Etiam pretium diam diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin rutrum luctus velit, vel euismod est venenatis non. Donec quis nulla ut neque vulputate elementum eget vitae enim. Praesent scelerisque velit maximus, tempor ligula sit amet, fringilla mi. Vivamus consectetur purus in risus semper hendrerit."
            ],
        ];
    }
}
