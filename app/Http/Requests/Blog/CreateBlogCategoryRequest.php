<?php

namespace App\Http\Requests\Blog;

use Illuminate\Foundation\Http\FormRequest;

class CreateBlogCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|string|min:3"
        ];
    }

    public function bodyParameters()
    {
        return [
            'name' => [
                'description' => 'Nom de la catégorie',
                'example' => "Matériel Roulant"
            ],
        ];
    }
}
