<?php

namespace App\Http\Requests\Blog;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class BlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(Request::getMethod() == 'POST') {
            return [
                "title" => "required|string|min:5",
                "content" => "required|string",
                "blog_category_id" => "required"
            ];
        } else {
            return [
                "title" => "required|string|min:5",
                "content" => "required|string",
                "blog_category_id" => "required"
            ];
        }
    }

    public function bodyParameters()
    {
        return [
            'title' => [
                'description' => "Titre de l'article",
                'example' => "Ceci est un titre",
                "required"
            ],
            'content' => [
                "description" => "Contenue de l'article",
                "example" => "Ceci est la description d'un article avec beaucoups de caractères",
                "required"
            ],
            'blog_category_id' => [
                "description" => "ID de la catégorie parente",
                "required"
            ]
        ];
    }
}
