<?php

namespace App\Http\Controllers\Api\Mod;

use App\Http\Controllers\Controller;
use App\Http\Requests\Mod\ModFileRequest;
use App\Http\Resources\Api\Mod\ModFileGet;
use App\Http\Resources\Api\Mod\ModFileList;
use App\Models\Mod\ModFile;
use Illuminate\Http\Request;

/**
 * Class ModFileController
 * @group Mod File
 *
 * Gestion des fichiers d'un mod particulier
 *
 * @package App\Http\Controllers\Api\Mod
 */
class ModFileController extends Controller
{
    /**
     * @var ModFile
     */
    private $modFile;
    /**
     * @var ModFileList
     */
    private $list;
    /**
     * @var ModFileGet
     */
    private $get;

    /**
     * ModFileController constructor.
     * @param ModFile $modFile
     * @param ModFileList $list
     * @param ModFileGet $get
     */
    public function __construct(ModFile $modFile, ModFileList $list, ModFileGet $get)
    {
        $this->modFile = $modFile->newQuery();
        $this->list = $list;
        $this->get = $get;
    }

    /**
     * Listing Mod File
     * Affiche la liste global des fichiers d'un mod particulier
     *
     * @urlParam $modid int required ID du mod. example: 1
     *
     * @param $modid
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($modid)
    {
        try {
            $files = $this->modFile->where('mod_id', $modid)->get();

            return api()->response(200, "Listing Mod Files", $this->list->toArray($files));
        }catch (\Exception $exception) {
            return api()->error("Erreur lors de l'affichage des fichiers du mod'", [
                "code" => "ERROR-500-2021-03-24-006",
                "error" => $exception->getMessage(),
                "trace" => $exception->getTraceAsString()
            ]);
        }
    }

    /**
     * Filtering Listing mod file
     * Affiche la liste des fichiers d'un mod filtré
     *
     * @urlParam $modid int required ID du mod. Example: 1
     * @bodyParam type string|array Le ou les types de fichier rechercher. Example: image
     *
     * @param Request $request
     * @param $modid
     * @return \Illuminate\Http\JsonResponse
     */
    public function listing(Request $request, $modid)
    {
        try {
            $files = $this->modFile->where('mod_id', $modid);

            if($request->has('type')) {
                if(is_string($request->get('type'))) {
                    $files->where('type', $request->get('type'));
                } else {
                    $count = count($request->get('type'));
                    $files->where('type', $request->get('type')[0]);

                    for($i=1; $i < $count; $i++) {
                        $files->orWhere('type', $request->get('type')[$i]);
                    }
                }
            }

            return api()->response(200, "Mod File Listing Filtering", $this->list->toArray($files->get()));
        }catch (\Exception $exception) {
            return api()->error("Erreur lors de l'affichage des fichiers du mod filtrer'", [
                "code" => "ERROR-500-2021-03-24-013",
                "error" => $exception->getMessage(),
                "trace" => $exception->getTraceAsString()
            ]);
        }
    }

    /**
     * Create Mod File
     * Création d'un fichier d'un mod
     *
     * @urlParam $modid int required ID du mod. example: 1
     *
     * @param ModFileRequest $request
     * @param $modid
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ModFileRequest $request, $modid)
    {
        if($request->validated()) {
            try {
                $file = $this->modFile->create([
                    "nameFile" => $request->get('nameFile'),
                    "type" => $request->get('type'),
                    "mod_id" => $modid
                ]);

                return api()->response(200, "Mod File Created", $this->get->toArray($file));
            }catch (\Exception $exception) {
                return api()->error("Erreur lors de la création d'un fichiers du mod'", [
                    "code" => "ERROR-500-2021-03-24-007",
                    "error" => $exception->getMessage(),
                    "trace" => $exception->getTraceAsString()
                ]);
            }
        } else {
            return api()->validation("Erreur lors de la validation des informations", [
                "code" => "ERROR-500-2021-03-24-008",
            ]);
        }
    }

    /**
     * Get Mod File
     * Affiche les informations d'un fichier d'un mod
     *
     * @urlParam $modid int required Id du mod. Example: 1
     * @urlParam $id int required Id du fichier. Example: 1
     *
     * @param $modid
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($modid, $id)
    {
        try {
            $file = $this->modFile->find($id);

            return api()->response(200, "Showing Mod File", $this->get->toArray($file));
        }catch (\Exception $exception) {
            return api()->error("Erreur lors de la visualisation d'un fichiers du mod'", [
                "code" => "ERROR-500-2021-03-24-009",
                "error" => $exception->getMessage(),
                "trace" => $exception->getTraceAsString()
            ]);
        }
    }

    /**
     * Update Mod File
     * Met à jour un fichier d'un mod
     *
     * @urlParam $modid int required Id du mod. Example: 1
     * @urlParam $id int required Id du fichier. Example: 1
     *
     * @param ModFileRequest $request
     * @param $modid
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ModFileRequest $request, $modid, $id)
    {
        if($request->validated()) {
            try {
                $file = $this->modFile->find($id);
                $file->update([
                    "nameFile" => $request->get('nameFile'),
                    "type" => $request->get('type'),
                    "mod_id" => $modid
                ]);

                return api()->response(200, "Mod File Updated", $this->get->toArray($file));
            }catch (\Exception $exception) {
                return api()->error("Erreur lors de la mise à jours d'un fichiers du mod'", [
                    "code" => "ERROR-500-2021-03-24-010",
                    "error" => $exception->getMessage(),
                    "trace" => $exception->getTraceAsString()
                ]);
            }
        } else {
            return api()->validation("Erreur lors de la validation des informations", [
                "code" => "ERROR-500-2021-03-24-011",
            ]);
        }
    }

    /**
     * Delete Mod File
     * Supprime un fichier associé à un mod
     *
     * @urlParam $modid int required Id du mod. Example: 1
     * @urlParam $id int required Id du fichier. Example: 1
     *
     * @param $modid
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($modid, $id)
    {
        try {
            $this->modFile->find($id)->delete();

            return api()->response(200, "Mod File Deleted", null);
        }catch (\Exception $exception) {
            return api()->error("Erreur lors de la suppression d'un fichiers du mod'", [
                "code" => "ERROR-500-2021-03-24-012",
                "error" => $exception->getMessage(),
                "trace" => $exception->getTraceAsString()
            ]);
        }
    }
}
