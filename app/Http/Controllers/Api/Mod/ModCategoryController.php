<?php

namespace App\Http\Controllers\Api\Mod;

use App\Http\Controllers\Controller;
use App\Http\Requests\Mod\ModCategoryRequest;
use App\Http\Resources\Api\Mod\ModCategoryGet;
use App\Http\Resources\Api\Mod\ModCategoryList;
use App\Models\Mod\ModCategory;
use Illuminate\Http\Request;

/**
 * Class ModCategoryController
 * @group Mod Category
 *
 * Gestion des catégories de mod
 *
 * @package App\Http\Controllers\Api\Mod
 */
class ModCategoryController extends Controller
{
    /**
     * @var ModCategory
     */
    private $modCategory;
    /**
     * @var ModCategoryList
     */
    private $list;
    /**
     * @var ModCategoryGet
     */
    private $get;

    /**
     * ModCategoryController constructor.
     * @param ModCategory $modCategory
     * @param ModCategoryList $list
     * @param ModCategoryGet $get
     */
    public function __construct(ModCategory $modCategory, ModCategoryList $list, ModCategoryGet $get)
    {
        $this->modCategory = $modCategory->newQuery();
        $this->list = $list;
        $this->get = $get;
    }

    /**
     * List Mod Category
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            $categories = $this->modCategory->get();

            return api()->response(200, "Listing Mod Category", $this->list->toArray($categories));
        } catch (\Exception $exception) {
            return api()->error("Erreur lors de l'affichage des catégories de mod", [
                "code" => "ERROR-500-2021-03-23-001",
                "error" => $exception->getMessage(),
                "trace" => $exception->getTraceAsString()
            ]);
        }
    }

    /**
     * Create Mod Category
     *
     * @param ModCategoryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ModCategoryRequest $request)
    {
        if ($request->validated()) {
            try {
                $category = $this->modCategory->create([
                    "name" => $request->get('name')
                ]);

                return api()->response(200, "Mod Category Created", $this->get->toArray($category));
            } catch (\Exception $exception) {
                return api()->error("Erreur lors de la création d'une catégorie de mod", [
                    "code" => "ERROR-500-2021-03-23-002",
                    "error" => $exception->getMessage(),
                    "trace" => $exception->getTraceAsString()
                ]);
            }
        } else {
            return api()->validation("Erreur lors de la validation des informations de création de la catégorie de mod", [
                "code" => "ERROR-500-2021-03-23-003",
            ]);
        }
    }

    /**
     * Get Mod Category
     *
     * @urlParam $id int required Id de la catégorie
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            $category = $this->modCategory->find($id);

            return api()->response(200, "Get Category Mod", $this->get->toArray($category));
        } catch (\Exception $exception) {
            return api()->error("Erreur lors de l'affichage de la catégorie de mod", [
                "code" => "ERROR-500-2021-03-23-004",
                "error" => $exception->getMessage(),
                "trace" => $exception->getTraceAsString()
            ]);
        }
    }

    /**
     * Update Mod Category
     *
     * @urlParam $id int required Id de la catégorie
     *
     * @param ModCategoryRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ModCategoryRequest $request, $id)
    {
        if ($request->validated()) {
            try {
                $category = $this->modCategory->find($id);
                $category->update($request->all());

                return api()->response(200, "Update Category Mod", $this->get->toArray($category));
            } catch (\Exception $exception) {
                return api()->error("Erreur lors de la mise à jour de la catégorie de mod", [
                    "code" => "ERROR-500-2021-03-23-005",
                    "error" => $exception->getMessage(),
                    "trace" => $exception->getTraceAsString()
                ]);
            }
        } else {
            return api()->validation("Erreur lors de la validation des informations de la mise à jour de la catégorie de mod", [
                "code" => "ERROR-500-2021-03-23-006",
            ]);
        }
    }

    /**
     * Delete Mod Category
     *
     * @urlParam $id int required Id de la catégorie
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $this->modCategory->find($id)->delete();

            return api()->response(200, "Delete Category Mod", null);
        } catch (\Exception $exception) {
            return api()->error("Erreur lors de la suppression de la catégorie de mod", [
                "code" => "ERROR-500-2021-03-23-007",
                "error" => $exception->getMessage(),
                "trace" => $exception->getTraceAsString()
            ]);
        }
    }
}
