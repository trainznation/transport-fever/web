<?php

namespace App\Http\Controllers\Api\Mod;

use App\Http\Controllers\Controller;
use App\Http\Requests\Mod\ModNewsRequest;
use App\Http\Resources\Api\Mod\ModNewsGet;
use App\Http\Resources\Api\Mod\ModNewsList;
use App\Models\Mod\ModNews;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

/**
 * Class ModNewsController
 * @group Mod News
 *
 * Gestion des News d'un mod particulier
 *
 * @package App\Http\Controllers\Api\Mod
 */
class ModNewsController extends Controller
{
    /**
     * @var ModNews
     */
    private $modNews;
    /**
     * @var ModNewsList
     */
    private $list;
    /**
     * @var ModNewsGet
     */
    private $get;

    /**
     * ModNewsController constructor.
     * @param ModNews $modNews
     * @param ModNewsList $list
     * @param ModNewsGet $get
     */
    public function __construct(ModNews $modNews, ModNewsList $list, ModNewsGet $get)
    {
        $this->modNews = $modNews->newQuery();
        $this->list = $list;
        $this->get = $get;
    }

    /**
     * Listing Mod News
     * Affiche la liste des news d'un mod particulier
     *
     * @urlParam $modid int required ID du mod
     *
     * @param $modid
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($modid)
    {
        $news = $this->modNews->where("mod_id", $modid)->get();

        return api()->response(200, "Listing Mod News", $this->list->toArray($news));
    }

    /**
     * Listing Mod News Filtering
     * Affiche la liste des news d'un mod particulier filtrer
     *
     * @bodyParam $limit int Nombre limit de donnée à afficher. Example: 5
     * @bodyParam $state int Etat des news à afficher. Example: 2
     * @bodyParam $order string Colonne de filtrage particulier. Example: published_at
     * @bodyParam $sort string Ordre dans lesquelle les données s'affiche (Fonctionne de père avec $order). Example: asc
     *
     * @urlParam $modid int required ID du mod. Example: 1
     *
     * @param Request $request
     * @param $modid
     * @return \Illuminate\Http\JsonResponse
     */
    public function listing(Request $request, $modid)
    {
        $news = $this->modNews->where('mod_id', $modid);

        if($request->get('limit') !== null) {
            $news->limit($request->get('limit'));
        }

        if($request->get('state') !== null) {
            $news->where('state', $request->get('state'));
        }

        if($request->get('order') !== null && $request->get('sort') !== null) {
            $news->orderBy($request->get('order'), $request->get('sort'));
        }

        return api()->response(200, "Listing Mod News Filtering", $this->list->toArray($news->get()));
    }

    /**
     * Create Mod News
     * Création d'une nouvelle pour un mod particulier
     *
     * @urlParam $modid int required ID du mod possédant la news. Example: 1
     *
     * @param ModNewsRequest $request
     * @param $modid
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ModNewsRequest $request, $modid)
    {
        if ($request->validated()) {
            try {
                $news = $this->modNews->create([
                    "title" => $request->title,
                    "slug" => Str::slug($request->title),
                    "content" => $request->get('content'),
                    "state" => ($request->has('state')) ? $request->state : 0,
                    "published_at" => ($request->get('state') == 2) ? $request->get('published_at') : (($request->get('state') == 1) ? now() : null),
                    "mod_id" => $modid
                ]);

                return api()->response(200, "Mod News Created", $this->get->toArray($news));
            } catch (\Exception $exception) {
                return api()->error("Erreur lors de la création de la news", [
                    "code" => "ERROR-500-2021-03-24-001",
                    "error" => $exception->getMessage(),
                    "trace" => $exception->getTraceAsString()
                ]);
            }
        } else {
            return api()->validation("Erreur lors de la validation des informations", [
                "code" => "ERROR-500-2021-03-24-002",
            ]);
        }
    }

    /**
     * Get Mod News
     * Affiche une news d'un mod particulier
     *
     * @urlParam $modid int required ID du mod. Example: 1
     * @urlParam $id int required ID de la news à afficher. Example: 1
     *
     * @param $modid
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($modid, $id)
    {
        try {
            $news = $this->modNews->newQuery()->find($id);

            return api()->response(200, "Showing Mod News", $this->get->toArray($news));
        }catch (\Exception $exception) {
            return api()->error("Erreur lors de l'affichage de la news", [
                "code" => "ERROR-500-2021-03-24-003",
                "error" => $exception->getMessage(),
                "trace" => $exception->getTraceAsString()
            ]);
        }
    }

    /**
     * Update Mod News
     * Met à jours les informations d'une news d'un mod particulier
     *
     * @urlParam $modid int required ID du mod. Example: 1
     * @urlParam $id int required ID de la news à afficher. Example: 1
     *
     * @param ModNewsRequest $request
     * @param $modid
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ModNewsRequest $request, $modid, $id)
    {
        if($request->validated()) {
            try {
                $news = $this->modNews->find($id);
                $news->update([
                    "title" => $request->title,
                    "slug" => Str::slug($request->title),
                    "content" => $request->get('content'),
                    "state" => ($request->has('state')) ? $request->state : 0,
                    "published_at" => ($request->get('state') == 2) ? $request->get('published_at') : (($request->get('state') == 1) ? now() : null),
                    "mod_id" => $modid
                ]);

                return api()->response(200, "Updated Mod News", $this->get->toArray($news));
            }catch (\Exception $exception) {
                return api()->error("Erreur lors de la mise à jour de la news", [
                    "code" => "ERROR-500-2021-03-24-004",
                    "error" => $exception->getMessage(),
                    "trace" => $exception->getTraceAsString()
                ]);
            }
        } else {
            return api()->validation("Erreur lors de la validation des informations", [
                "code" => "ERROR-500-2021-03-24-005",
            ]);
        }
    }

    /**
     * Delete Mod News
     * Supprime une news d'un mod particulier
     *
     * @urlParam $modid int required ID du mod. Example: 1
     * @urlParam $id int required ID de la news à afficher. Example: 1
     *
     * @param $modid
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($modid, $id)
    {
        try {
            $this->modNews->find($id)->delete();

            return api()->response(200, "Deleted Mod News", null);
        }catch (\Exception $exception) {
            return api()->error("Erreur lors de la mise à jour de la news", [
                "code" => "ERROR-500-2021-03-24-004",
                "error" => $exception->getMessage(),
                "trace" => $exception->getTraceAsString()
            ]);
        }
    }
}
