<?php

namespace App\Http\Controllers\Api\Mod;

use App\Http\Controllers\Controller;
use App\Http\Requests\Mod\ModRequest;
use App\Http\Resources\Api\Mod\ModGet;
use App\Http\Resources\Api\Mod\ModList;
use App\Models\Mod\Mod;
use Illuminate\Http\Request;


/**
 * Class ModController
 * @group Mod
 * @package App\Http\Controllers\Api\Mod
 */
class ModController extends Controller
{
    /**
     * @var Mod
     */
    private $mod;
    /**
     * @var ModList
     */
    private $list;
    /**
     * @var ModGet
     */
    private $get;

    /**
     * ModController constructor.
     * @param Mod $mod
     * @param ModList $list
     * @param ModGet $get
     */
    public function __construct(Mod $mod, ModList $list, ModGet $get)
    {
        $this->mod = $mod->newQuery();
        $this->list = $list;
        $this->get = $get;
    }

    /**
     * List Mod
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            $mods = $this->mod->get();

            return api()->response(200, "Listing Mod", $this->list->toArray($mods));
        } catch (\Exception $exception) {
            return api()->error("Erreur lors de l'affichage de la liste des mods", [
                "code" => "ERROR-500-2021-03-23-008",
                "error" => $exception->getMessage(),
                "trace" => $exception->getTraceAsString()
            ]);
        }
    }

    /**
     * Listing Mod Filtering
     *
     * @bodyParam $category int ID de la catégorie rechercher
     * @bodyParam $published int Etat du mod rechercher. Example: 2
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function listing(Request $request)
    {
        $mods = $this->mod;

        if($request->get('category')) {
            $mods->where('mod_category_id', $request->get('category'));
        }

        if($request->get('published')) {
            $mods->where('published', $request->get('published'));
        }

        return api()->response(200, "Listing Mod Filtering", $this->list->toArray($mods->get()));
    }

    /**
     * Create Mod
     *
     * @param ModRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ModRequest $request)
    {
        if ($request->validated()) {
            try {
                $mod = $this->mod->create($request->all());

                return api()->response(200, "Mod Created", $this->get->toArray($mod));
            } catch (\Exception $exception) {
                return api()->error("Erreur lors de la création du mods", [
                    "code" => "ERROR-500-2021-03-23-009",
                    "error" => $exception->getMessage(),
                    "trace" => $exception->getTraceAsString()
                ]);
            }
        } else {
            return api()->validation("Erreur lors de la validation des informations de la création du mod", [
                "code" => "ERROR-500-2021-03-23-010",
            ]);
        }
    }

    /**
     * Get Mod
     *
     * @urlParam $id int required ID du mod. Example: 1
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            $mod = $this->mod->find($id);

            return api()->response(200, "Get Mod", $this->get->toArray($mod));
        } catch (\Exception $exception) {
            return api()->error("Erreur lors de l'affichage du mods", [
                "code" => "ERROR-500-2021-03-23-011",
                "error" => $exception->getMessage(),
                "trace" => $exception->getTraceAsString()
            ]);
        }
    }

    /**
     * Update Mod
     *
     * @urlParam $id int required ID du mod. Example: 3
     *
     * @param ModRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ModRequest $request, $id)
    {
        if ($request->validated()) {
            try {
                $mod = $this->mod->find($id);
                $mod->update($request->all());

                return api()->response(200, "Mod Updated", $this->get->toArray($mod));
            } catch (\Exception $exception) {
                return api()->error("Erreur lors de la mise à jour du mods", [
                    "code" => "ERROR-500-2021-03-23-012",
                    "error" => $exception->getMessage(),
                    "trace" => $exception->getTraceAsString()
                ]);
            }
        } else {
            return api()->validation("Erreur lors de la validation des informations de la mise à jour du mod", [
                "code" => "ERROR-500-2021-03-23-013",
            ]);
        }
    }

    /**
     * Delete Mod
     *
     * @urlParam $id int required ID du mod
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $this->mod->find($id)->delete();

            return api()->response(200, "Mod Deleted", null);
        } catch (\Exception $exception) {
            return api()->error("Erreur lors de la suppression du mods", [
                "code" => "ERROR-500-2021-03-23-014",
                "error" => $exception->getMessage(),
                "trace" => $exception->getTraceAsString()
            ]);
        }
    }
}
