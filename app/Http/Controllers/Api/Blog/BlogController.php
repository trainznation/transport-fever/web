<?php

namespace App\Http\Controllers\Api\Blog;

use App\Http\Controllers\Controller;
use App\Http\Requests\Blog\BlogRequest;
use App\Http\Resources\Api\Blog\BlogGet;
use App\Http\Resources\Api\Blog\BlogList;
use App\Models\Blog\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

/**
 * Class BlogController
 * @group Blog Management
 *
 * Gestion du Blog
 *
 * @package App\Http\Controllers\Api\Blog
 */
class BlogController extends Controller
{
    /**
     * @var Blog
     */
    private $blog;
    /**
     * @var BlogList
     */
    private $list;
    /**
     * @var BlogGet
     */
    private $get;

    /**
     * BlogController constructor.
     * @param Blog $blog
     * @param BlogList $list
     * @param BlogGet $get
     */
    public function __construct(Blog $blog, BlogList $list, BlogGet $get)
    {
        $this->blog = $blog->newQuery();
        $this->list = $list;
        $this->get = $get;
    }

    /**
     * List Post
     *
     * Affiche la liste des articles
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $posts = $this->blog;

        return api()->response(200, 'List All Post', $this->list->toArray($posts->get()));
    }

    /**
     * Listing Filter Article
     *
     * @bodyParam $category int ID de la catégorie à rechercher. Example: 1
     * @bodyParam $order string Ordre de la recherche. Example: asc
     * @bodyParam $limit int Limit à afficher de la recherche. Example: 5
     * @bodyParam $published int Etat de la publication (0,1 ou 2). Example: 1
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function listing(Request $request)
    {
        //dd($request->all());
        $posts = $this->blog;

        if ($request->get('category')) {
            $posts->where('blog_category_id', $request->get('category'));
        }

        if ($request->get('order')) {
            $posts->orderBy('id', $request->get('order'));
        }

        if ($request->get('limit')) {
            $posts->limit($request->get('limit'));
        }

        if ($request->get('published')) {
            $posts->where('state', $request->get('published'));
        }

        return api()->response(200, 'List Post', $this->list->toArray($posts->get()));
    }

    /**
     * Create Post
     *
     * Création d'un article
     *
     *
     * @param BlogRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(BlogRequest $request)
    {
        if ($request->validated()) {
            try {
                $post = $this->blog->create([
                    "title" => $request->get('title'),
                    "slug" => Str::slug($request->get('title')),
                    "content" => $request->get('content'),
                    "state" => $request->has('state') ? $request->get('state') : 0,
                    "published_at" => ($request->get('state') == 2) && $request->has('published_at') ? $request->get('published_at') : null,
                    "blog_category_id" => $request->get('blog_category_id')
                ]);
                return api()->response(200, 'Post Created', $this->get->toArray($post));
            } catch (\Exception $exception) {
                return api()->error("Erreur lors de la création du post", [
                    "code" => "ERROR-500-2021-03-18-002",
                    "error" => $exception->getMessage(),
                    "trace" => $exception->getTraceAsString()
                ]);
            }
        } else {
            return api()->validation('Les champs suivant sont invalide', ['title', 'content', 'blog_category_id']);
        }
    }

    /**
     * Get Post
     *
     * Affiche un article particulier
     *
     * @urlParam $post int required ID de l'article à afficher
     *
     * @param \App\Models\Blog\Blog $post
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Blog $post)
    {
        try {
            return api()->response(200, 'Post', $this->get->toArray($post));
        } catch (\Exception $exception) {
            return api()->error("Erreur lors de l'affichage du post", [
                "code" => "ERROR-500-2021-03-18-003",
                "error" => $exception->getMessage(),
                "trace" => $exception->getTraceAsString()
            ]);
        }
    }

    /**
     * Update Post
     *
     * Met à jour un article particulier
     *
     * @urlParam $post int required ID de l'article à modifier
     *
     * @param BlogRequest $request
     * @param \App\Models\Blog\Blog $post
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(BlogRequest $request, Blog $post)
    {
        if ($request->validated()) {
            try {
                $post->update([
                    "title" => $request->get('title'),
                    "slug" => Str::slug($request->get('title')),
                    "content" => $request->get('content'),
                    "state" => $request->has('state') ? $request->get('state') : 0,
                    "published_at" => ($request->get('state') == 2) ? $request->get('published_at') : (($request->get('state') == 1) ? now() : null),
                    "blog_category_id" => $request->get('blog_category_id')
                ]);

                return api()->response(200, "Post Updated", $this->get->toArray($post));
            } catch (\Exception $exception) {
                return api()->error("Erreur lors de la mise à jour du post", [
                    "code" => "ERROR-500-2021-03-18-004",
                    "error" => $exception->getMessage(),
                    "trace" => $exception->getTraceAsString()
                ]);
            }
        } else {
            return api()->validation('Les champs suivant sont invalide', ['title', 'content', 'blog_category_id']);
        }
    }

    /**
     * Delete Post
     *
     * Supprime un article
     *
     * @urlParam $post int required ID de l'article à supprimer
     *
     * @param \App\Models\Blog\Blog $post
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Blog $post)
    {
        try {
            $post->delete();
            return api()->response(200, "Post Deleted", []);
        } catch (\Exception $exception) {
            return api()->error("Erreur lors de la mise à jour du post", [
                "code" => "ERROR-500-2021-03-18-005",
                "error" => $exception->getMessage(),
                "trace" => $exception->getTraceAsString()
            ]);
        }
    }
}
