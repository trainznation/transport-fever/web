<?php

namespace App\Http\Controllers\Api\Blog;

use API;
use App\Http\Controllers\Controller;
use App\Http\Requests\Blog\CreateBlogCategoryRequest;
use App\Http\Resources\Api\Blog\BlogCategoryGet;
use App\Http\Resources\Api\Blog\BlogCategoryList;
use App\Models\Blog\BlogCategory;
use Illuminate\Http\Request;

/**
 * Class BlogCategoryController
 * @group Blog Management
 *
 * Gestion des catégories du blog
 *
 * @package App\Http\Controllers\Api\Blog
 */
class BlogCategoryController extends Controller
{
    /**
     * @var BlogCategory
     */
    private $blogCategory;
    /**
     * @var BlogCategoryList
     */
    private $blogCategoryList;
    /**
     * @var BlogCategoryGet
     */
    private $blogCategoryGet;

    /**
     * BlogCategoryController constructor.
     * @hideFromAPIDocumentation
     * @param BlogCategory $blogCategory
     * @param BlogCategoryList $blogCategoryList
     * @param BlogCategoryGet $blogCategoryGet
     */
    public function __construct(BlogCategory $blogCategory, BlogCategoryList $blogCategoryList, BlogCategoryGet $blogCategoryGet)
    {
        $this->blogCategory = $blogCategory->newQuery();
        $this->blogCategoryList = $blogCategoryList;
        $this->blogCategoryGet = $blogCategoryGet;
    }

    /**
     * List Category
     *
     * Affiche la liste des categories du blog
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $blogs = $this->blogCategory->get();

        return api()->response(200, 'Blog Category List', $this->blogCategoryList->toArray($blogs));
    }

    /**
     * Create Category
     *
     * @responseField name string Nom de la catégorie
     * @responseField id Id de la categorie
     * @param CreateBlogCategoryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateBlogCategoryRequest $request)
    {


        api()->validation('Ces champs sont requis', ['name']);
        api()->validation('Le champs doit être une chaine de caractère', ['name']);
        api()->validation("Le champs doit être d'au moins 3 caractères", ['name']);

        $category = $this->blogCategory->create([
            "name" => $request->name
        ]);

        return api()->response(200, 'Create Blog Category', $this->blogCategoryGet->toArray($category));
    }

    /**
     * Get Category
     *
     * Affiche les informations d'une categorie particulière
     *
     * @urlParam category int required Id de la category.
     *
     * @param BlogCategory $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(BlogCategory $category)
    {
        return api()->response(200, 'Get Category', $this->blogCategoryGet->toArray($category));
    }

    /**
     * Update Category
     *
     * Mise à jour d'une catégorie
     *
     * @urlParam $category int required Id de la category à modifier.
     *
     * @param CreateBlogCategoryRequest $request
     * @param BlogCategory $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CreateBlogCategoryRequest $request, BlogCategory $category)
    {
        $category->update(["name" => $request->get('name')]);

        return api()->response(200, 'Update Category', $this->blogCategoryGet->toArray($category));
    }

    /**
     * Delete Category
     *
     * Supprime une categorie
     *
     * @urlParam $category int required ID de la catégorie.
     *
     * @param BlogCategory $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(BlogCategory $category)
    {
        try {
            $category->delete();

            return api()->response(200, 'Delete Category', []);
        } catch (\Exception $exception) {
            return api()->response(500, "Delete Category", [], [
                "code" => "ERROR-500-2021-03-18-001",
                "trace" => $exception->getTraceAsString(),
                "error" => $exception->getMessage()
            ]);
        }
    }
}
