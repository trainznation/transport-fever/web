<?php

namespace App\Http\Controllers\Api\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\SettingAnnounceRequest;
use App\Http\Resources\Api\Settings\AnnouncementGet;
use App\Http\Resources\Api\Settings\AnnouncementList;
use App\Models\Setting\Announcement;
use Illuminate\Http\Request;

/**
 * Class AnnouncementController
 * @group Setting Annonce
 *
 * Gestionnaire d'annonce du site internet
 *
 * @package App\Http\Controllers\Api\Setting
 */
class AnnouncementController extends Controller
{
    /**
     * @var Announcement
     */
    private $announcement;
    /**
     * @var AnnouncementList
     */
    private $announcementList;
    /**
     * @var AnnouncementGet
     */
    private $announcementGet;

    /**
     * AnnouncementController constructor.
     * @param Announcement $announcement
     * @param AnnouncementList $announcementList
     * @param AnnouncementGet $announcementGet
     */
    public function __construct(Announcement $announcement, AnnouncementList $announcementList, AnnouncementGet $announcementGet)
    {
        $this->announcement = $announcement;
        $this->announcementList = $announcementList;
        $this->announcementGet = $announcementGet;
    }

    /**
     * List Annonces
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            $announces = $this->announcement->newQuery()->get();
            return api()->response(200, "List Announcement", $this->announcementList->toArray($announces));
        } catch (\Exception $exception) {
            return api()->error("Erreur lors de l'affichage des annonces", [
                "code" => "ERROR-500-2021-03-19-001",
                "error" => $exception->getMessage(),
                "trace" => $exception->getTraceAsString()
            ]);
        }
    }

    /**
     * Create Announce
     *
     * Création d'une Annonce
     *
     * @param SettingAnnounceRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(SettingAnnounceRequest $request)
    {
        if ($request->validated()) {
            try {
                $announce = $this->announcement->newQuery()->create([
                    "title" => $request->get('title'),
                    "type" => $request->get('type'),
                    "content" => $request->get('content') ? $request->get('content') : null,
                    "expiring_at" => $request->get('expiring_at') ? $request->get('expiring_at') : now()->addDays(3)
                ]);

                return api()->response(200, "Create Announcement", $this->announcementGet->toArray($announce));
            } catch (\Exception $exception) {
                return api()->error("Erreur lors de la création de l'annonces", [
                    "code" => "ERROR-500-2021-03-19-002",
                    "error" => $exception->getMessage(),
                    "trace" => $exception->getTraceAsString()
                ]);
            }
        } else {
            return api()->validation('Les champs suivant sont invalide', ['title', 'type']);
        }
    }

    /**
     * Get Announce
     *
     * Affiche les informations de l'annonce rechercher.
     * L'affichage n'est pas obligatoire étant donner que le contenue lors de la création ne l'est pas non plus.
     *
     * @urlParam $announce int required ID de l'annonce à afficher
     *
     * @param Announcement $announce
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Announcement $announce)
    {
        try {
            $announce = $this->announcement->newQuery()->find($announce)->first();

            return api()->response(200, "Get Announce", $this->announcementGet->toArray($announce));
        } catch (\Exception $exception) {
            return api()->error("Erreur lors de l'affichage de l'annonces", [
                "code" => "ERROR-500-2021-03-19-003",
                "error" => $exception->getMessage(),
                "trace" => $exception->getTraceAsString()
            ]);
        }
    }

    /**
     * Delete Announce
     *
     * Supprime l'annonce emis en paramètre.
     * L'annonce peut également être supprimer par l'intermédiaire de la console: php artisan announce:delete --id=?
     *
     * @urlParam $announce int required ID de l'annonce à supprimer
     *
     * @param Announcement $announce
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Announcement $announce)
    {
        try {
            $this->announcement->newQuery()->find($announce)->first()->delete();

            return api()->response(200, "Delete Announce", null);
        }catch (\Exception $exception) {
            return api()->error("Erreur lors de la suppression de l'annonces", [
                "code" => "ERROR-500-2021-03-19-004",
                "error" => $exception->getMessage(),
                "trace" => $exception->getTraceAsString()
            ]);
        }
    }
}
