<?php

namespace App\Http\Controllers;

use App\Http\Resources\InsightResources;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use LarapexChart;

class InsightController extends Controller
{
    public function index()
    {
        $insight = new InsightResources();
        $response = \Storage::disk('public')->get('insight.json');
        $array = json_decode($response);

        //dd($insight->toArray($array));

        $code = LarapexChart::radialChart()
            ->setTitle('Code Average')
            ->addData([$array->summary->code])
            ->setLabels(["Code"])
            ->setColors([$this->color($array->summary->code)])
            ->setWidth(280)
            ->setHeight(280);

        $complexity = LarapexChart::radialChart()
            ->setTitle('Complexity Average')
            ->addData([$array->summary->complexity])
            ->setLabels(["Complexité"])
            ->setColors([$this->color($array->summary->complexity)])
            ->setWidth(280)
            ->setHeight(280);

        $archi = LarapexChart::radialChart()
            ->setTitle('Architecture Average')
            ->addData([$array->summary->architecture])
            ->setLabels(["Architecture"])
            ->setColors([$this->color($array->summary->architecture)])
            ->setWidth(280)
            ->setHeight(280);

        $style = LarapexChart::radialChart()
            ->setTitle('Style Average')
            ->addData([$array->summary->style])
            ->setLabels(["Style"])
            ->setColors([$this->color($array->summary->style)])
            ->setWidth(280)
            ->setHeight(280);

        return view('insight.index', compact('code', 'complexity', 'archi', 'style'), [
            "array" => $insight->toArray($array)
        ]);

    }

    private function color($average)
    {
        if ($average <= 33.33) {
            return '#f44336';
        } elseif ($average > 33.33 && $average <= 66.67) {
            return '#FF5722';
        } else {
            return '#4CAF50';
        }
    }
}
