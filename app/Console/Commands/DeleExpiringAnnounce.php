<?php

namespace App\Console\Commands;

use App\Models\Setting\Announcement;
use Illuminate\Console\Command;

class DeleExpiringAnnounce extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'announce:expiring';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Supprime les annonces expirés';
    /**
     * @var Announcement
     */
    private $announcement;

    /**
     * Create a new command instance.
     *
     * @param Announcement $announcement
     */
    public function __construct(Announcement $announcement)
    {
        parent::__construct();
        $this->announcement = $announcement;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $announces = $this->announcement->newQuery()->where('expiring_at', '<=', now())->get();
        $i = 0;
        try {
            foreach ($announces as $announce) {
                $this->announcement->newQuery()->find($announce->id)->first()->delete();
                $i++;
            }
            $this->info("Annonce supprimer avec succès: ".$i);
            \Log::info("Annonce supprimer avec succès: ".$i);
        }catch (\Exception $exception) {
            $this->error($exception->getMessage());
            \Log::error($exception->getMessage());
        }

        return null;
    }
}
