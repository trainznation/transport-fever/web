<?php

namespace App\Console\Commands;

use App\Models\Setting\Announcement;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class DeleAnnounce extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'announce:delete {--id= : ID de l\'annonce à supprimer}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Supprime une annonce particulière';
    /**
     * @var Announcement
     */
    private $announcement;

    /**
     * Create a new command instance.
     *
     * @param Announcement $announcement
     */
    public function __construct(Announcement $announcement)
    {
        parent::__construct();
        $this->announcement = $announcement;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $announceId = $this->option('id');

        try {
            $this->announcement->newQuery()->find($announceId)->delete();
            $this->info("Annonce supprimer");
            Log::info("Annonce Supprimer");
        }catch (\Exception $exception) {
            $this->error($exception->getMessage());
            Log::error($exception->getMessage());
        }

        return null;
    }
}
